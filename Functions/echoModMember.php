<?php

function echoModMember($memberNo, $connection)
{
	$result = mysqli_query($connection, 'SELECT * FROM CSE_Members WHERE No=\'' . $memberNo . '\'');
	if (mysqli_num_rows($result) == 1) {
		$row = mysqli_fetch_assoc($result);
	?>
		<h1>修改成員資料-<?php echo $row['Name'] ?></h1>
		<div class="addNewsForm">
			<form action="../modmember.php" method="post" enctype="multipart/form-data">
				<div class="controls">
					<label for="Name">姓名</label>
					<input name="Name" type="text" placeholder="請輸入姓名" value="<?php echo $row['Name'] ?>" required />
				</div>
				<div class="controls">
					<label for="Info">成員資訊</label>
					<textarea name="Info" placeholder="請輸入成員資訊" wrap="virtual" draggable="false" required><?php echo $row['Info'] ?></textarea>
				</div>
				<div class="controls">
					<label for="Email">Email</label>
					<input name="Email" type="text" placeholder="請輸入Email" value="<?php echo $row['Email'] ?>" required />
				</div>
				<div class="controls">
					<label for="Upload">個人照片</label>
					<br />
					<input name="Upload" type="file" />
				</div>
				<input name="ModMemberNo" type="hidden" value="<?php echo $memberNo ?>" />
				<div class="controls">
					<input type="submit" value="送出"></input>
					<a href="index.php">
						<span class="Cancel">取消</span>
					</a>
				</div>
			</form>
		</div>
	<?php
	} else {
		header("Location: index.php?msg=nodata");
	}
}

?>