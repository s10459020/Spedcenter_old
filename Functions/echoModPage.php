<?php

/**
 * 公告修改 ??
 */
function echoModPage($connection, $modNewsNo)
{
	$result = mysqli_query($connection, "SELECT * FROM CSE_News WHERE No = '" . $modNewsNo . "'");
	if (mysqli_num_rows($result) == 0) {
		header("Location: index.php?msg=nodata");
		exit();
	} else {
		$row = mysqli_fetch_assoc($result);
		?>
		<h1>修改公告</h1>
		<div class="addNewsForm">
			<form action="../modnews.php" method="post" enctype="multipart/form-data">
				<div class="controls">
					<label for="Title">標題</label>
					<input name="Title" type="text" placeholder="請輸入標題" value="<?php echo $row['Title'] ?>" required />
				</div>
				<div class="controls">
					<label for="Date">日期</label>
					<input name="Date" type="date" value="<?php echo $row['Date'] ?>" required />
				</div>
				<div class="controls">
					<label for="Study">研習活動</label>
					<input id="Study" name="Study" <?php if ($row['Type'] == 1) { ?> checked <?php } ?> type="checkbox" />
				</div>
				<div class="controls">
					<input name="Upload[]" type="file" multiple />
				</div>
				<div class="controls">
					<input id="link" type="text" placeholder="輸入欲插入的超連結網址" /><input type="button" onClick="insertLink()" value="插入超連結">
				</div>
				<script>
					function insertLink() {
						let textbox = document.getElementById("textbox");
						let link = document.getElementById("link");
						let l = link.value;
						textbox.value = textbox.value.substring(0, textbox.selectionEnd) + "<a href=\"" + l + "\">" + l + "</a>" + textbox.value.substring(textbox.selectionEnd, textbox.value.length);
						link.value = "";
						textbox.focus();
					}
				</script>
				<div class="controls">
					<textarea id="textbox" name="Content" placeholder="請輸入公告內文" wrap="virtual" draggable="false"><?php echo $row['Content'] ?></textarea>
				</div>
				<input type="hidden" name="ModNewsNo" value="<?php echo $modNewsNo ?>" />
				<div class="controls">
					<input type="submit" value="送出"></input>
					<a href="index.php">
						<span class="Cancel">取消</span>
					</a>
				</div>
			</form>
		</div>
	<?php
	}
}

?>