<?php

function echoFixPage($type, $connection)
{
	switch ($type) {
		case 'aboutCSE':
			require("fixpage/aboutCSE.php");
			break;
		case 'actandinfo':
			require("fixpage/actandinfo.php");
			break;
		case 'addNews':
			require("fixpage/addNews.php");
			break;
		case 'addmember':
			require("fixpage/addmember.php");
			break;
		case 'addcollection':
			require("fixpage/addcollection.php");
			break;
		case 'consultation':
			$result = mysqli_query($connection, "SELECT * FROM CSE_Fix_Contents WHERE No=7");
			require("fixpage/consultation.php");
			break;
		case 'collectionSelect':
			require("fixpage/collectionSelect.php");
			break;
		case 'collection':
			require("fixpage/collection.php");
			break;
		case 'feedback':
			require("fixpage/feedback.php");
			break;
		case 'modcollection':
			require("fixpage/modcollection.php");
			break;
		case 'netresource':
			require("fixpage/netresource.php");
			break;
		case 'siteMap':
			require("fixpage/siteMap.php");
			break;
		case 'introduction':
			$result = mysqli_query($connection, "SELECT * FROM CSE_Fix_Contents WHERE No>=1 AND No<=3 ORDER BY No");
			require("fixpage/introduction.php");
			break;
		case 'outline':
			$result = mysqli_query($connection, "SELECT * FROM CSE_Fix_Contents WHERE No>=4 AND No<=6 ORDER BY No");
			require("fixpage/outline.php");
			break;
		case 'testRegulation':
			$fileResult = mysqli_query($connection, "SELECT * FROM CSE_Fix_Contents WHERE No=8");
			$noteResult = mysqli_query($connection, "SELECT * FROM CSE_Fix_Contents WHERE No=9");
			$noteRows = mysqli_fetch_assoc($noteResult);

			$notes = explode("r`", $noteRows["Content"]);
			$temp = $notes[0];
			for ($i = 1; $i < count($notes); $i++) {
				$temp = $temp . '<span style="color:rgb(200,50,50);">' . $notes[$i];
			}
			
			$notes = explode("`r", $temp);
			$temp = $notes[0];
			for ($i = 1; $i < count($notes); $i++) {
				$temp = $temp . '</span>' . $notes[$i];
			}

			$notes = explode("R`", $temp);
			$temp = $notes[0];
			for ($i = 1; $i < count($notes); $i++) {
				$temp = $temp . '<span style="color:rgb(200,50,50);"><strong>' . $notes[$i];
			}
			
			$notes = explode("`R", $temp);
			$temp = $notes[0];
			for ($i = 1; $i < count($notes); $i++) {
				$temp = $temp . '</strong></span>' . $notes[$i];
			}
			
			$notes = explode("\n", $temp);
			require("fixpage/testRegulation.php");
			break;
		default:
	}
}

?>