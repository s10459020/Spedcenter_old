<?php

/**
 * 顯示Message Box
 */
function showMsgBox($msg)
{
	switch ($msg) {
		case 'accountnotfound':
			showMsgBoxCustom("找不到此帳號", "請檢查您的帳號密碼，並再試一次");
			break;
		case 'loginsuccess':
			showMsgBoxCustom("登入成功", "您已登入主控台");
			break;
		case 'pswdnotcorrect':
			showMsgBoxCustom("登入錯誤", "您輸入的密碼有誤，請再試一次");
			break;
		case 'nodata':
			showMsgBoxCustom("輸入錯誤", "並未接收到資訊");
			break;
		case 'logout':
			showMsgBoxCustom("登出成功", "您現在已登出主控台");
			break;
		case 'accessdenied':
			showMsgBoxCustom("存取失敗", "請確認您已登入主控台");
			break;
		case 'addnewssuccess':
			showMsgBoxCustom("新增成功", "公告已成功上傳");
			break;
		case 'addmembersuccess':
			showMsgBoxCustom("新增成功", "成員資料已成功上傳");
			break;
		case 'addcollectionsuccess':
			showMsgBoxCustom("新增成功", "書籍資料已成功上傳");
			break;
		case 'addnewssucuploadfailed':
			showMsgBoxCustom("新增失敗", "公告內容上傳成功，檔案上傳發生錯誤");
			break;
		case 'addmembersucuploadfailed':
			showMsgBoxCustom("新增失敗", "成員資料上傳成功，檔案上傳發生錯誤");
			break;
		case 'addcollectionsucuploadfailed':
			showMsgBoxCustom("新增失敗", "書籍資料上傳成功，檔案上傳發生錯誤");
			break;
		case 'addnewstitlefailed':
			showMsgBoxCustom("新增失敗", "公告列表上傳時發生資料庫錯誤");
			break;
		case 'delnewssuccess':
			showMsgBoxCustom("刪除成功", "公告已成功刪除");
			break;
		case 'delmembersuccess':
			showMsgBoxCustom("刪除成功", "成員資料已成功刪除");
			break;
		case 'delnewscontentfailed':
			showMsgBoxCustom("刪除失敗", "公告內容移除時發生資料庫錯誤");
			break;
		case 'delmemberfailed':
			showMsgBoxCustom("刪除失敗", "成員資料移除時發生錯誤錯誤");
			break;
		case 'addnewscontentfailed':
			showMsgBoxCustom("新增失敗", "公告上傳時發生錯誤");
			break;
		case 'addmembercontentfailed':
			showMsgBoxCustom("新增失敗", "成員資料上傳時發生錯誤");
			break;
		case 'addcollectioncontentfailed':
			showMsgBoxCustom("新增失敗", "書籍資料上傳時發生錯誤");
			break;
		case 'modfixsuccess':
			showMsgBoxCustom("修改成功", "頁面內容修改成功");
			break;
		case 'modfixfailed':
			showMsgBoxCustom("修改失敗", "頁面內容修改失敗");
			break;
		case 'addmembersucSameFile':
			showMsgBoxCustom("修改成功", "圖片名稱重複，將使用原名稱圖片，若要更換新圖片請更改檔名");
			break;
		case 'modconsultationimage':
			showMsgBoxCustom("修改成功", "圖片名稱重複，將使用原名稱圖片，若要更換新圖片請更改檔名");
			break;
		case 'uploadconsultationimagefailed':
			showMsgBoxCustom("修改失敗", "圖片上傳發生錯誤");
			break;
		case 'modconsultationimagefailed':
			showMsgBoxCustom("修改失敗", "變更圖片發生錯誤");
			break;
		case 'modtestregulationfile':
			showMsgBoxCustom("修改成功", "檔案名稱重複，將使用原名稱檔案，若要更換新檔案請更改檔名");
			break;
		case 'uploadtestregulationfilefailed':
			showMsgBoxCustom("修改失敗", "檔案上傳發生錯誤");
			break;
		case 'modtestregulationfilefailed':
			showMsgBoxCustom("修改失敗", "變更檔案發生錯誤");
			break;
		case 'delcollectionsuccess':
			showMsgBoxCustom("刪除成功", "書籍資料已成功刪除");
			break;
		case 'delcollectionfailed':
			showMsgBoxCustom("刪除失敗", "書籍資料刪除失敗");
			break;
		default:
			showMsgBoxCustom("custom", $msg);
	}
}

?>