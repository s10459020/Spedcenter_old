<h1>關於特教中心</h1>
<div class="mainBlockContent">
	<map>
		<a href="?page=introduction">
			<span class="guideItem">
				<label>中心簡介</label>
			</span>
		</a>
		<a href="?page=outline">
			<span class="guideItem">
				<label>業務概況</label>
			</span>
		</a>
		<a href="?page=consultation">
			<span class="guideItem">
				<label>諮詢服務</label>
			</span>
		</a>
		<a href="?page=collection">
			<span class="guideItem">
				<label>中心出版品</label>
			</span>
		</a>
		<a href="?page=testRegulation">
			<span class="guideItem">
				<label>測驗申請</label>
			</span>
		</a>
	</map>
</div>