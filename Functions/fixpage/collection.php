<?php 
	$classResult = mysqli_query($connection, "SELECT * FROM CSE_Collection GROUP BY Class");
	$classNum = mysqli_num_rows($classResult);
	
	if (isset($_GET['class'])) {
		$result = mysqli_query($connection, "SELECT * FROM CSE_Collection WHERE Class = '$_GET[class]' ORDER BY Sort");
	} else {
		$result = mysqli_query($connection, "SELECT * FROM CSE_Collection ORDER BY Sort");
	}
?>

<h1>中心出版品</h1>
<div class="mainBlockContent collectionBlock">
	<table class="text-xl">
		<tr>
			<?php
			if (isset($_SESSION['account'])) {
			?>
				<th></th>
			<?php
			}
			?>
			<th id="No">編號</th>
			<th id="Class">類別</th>
			<th id="Author">作者</th>
			<th id="BookName">書名</th>
			<th id="Remarks">備註</th>
			<?php
			if (isset($_SESSION['account'])) {
			?>
				<th></th>
			<?php
			}
			?>
		</tr>
		<?php
		$No = 0;
		while ($row = mysqli_fetch_assoc($result)) {
			$No += 1
		?>
			<tr>
				<?php
				if (isset($_SESSION['account'])) {
				?>
					<td><a href="?page=modcollection&no=<?php echo $row['No'] ?>">編輯</a></td>
				<?php
				}
				?>
				<td headers="No"><a onclick="changeNumber(<?php echo "$row[No], $row[Sort]"; ?>);"><?php echo $row["Sort"]; ?></a></td>
				<td headers="Class"><?php echo $row["Class"]; ?></td>
				<td headers="Author"><?php echo $row["Author"]; ?></td>
				<td headers="BookName" class="nametd">
					<?php
					if ($row["File"] != "") {
					?>
						<a href="../collection_appends/<?php echo $row["File"]; ?>" alt="下載檔案-<?php echo $row["File"]; ?>" download><?php echo $row["BookName"]; ?></a>
					<?php
					} else {
						echo $row["BookName"];
					}
					?>
				</td>
				<td headers="Remarks"><?php echo $row["Remarks"]; ?></td>
				<?php
				if (isset($_SESSION['account'])) {
				?>
					<td><a href="?msg=delcollection&delcollectionno=<?php echo $row['No'] ?>">刪除</a></td>
				<?php
				}
				?>
			</tr>

		<?php
		}
		?>
		<tr>
			<?php
			if (isset($_SESSION['account'])) {
			?>
				<td></td>
			<?php
			}
			?>
			<td colspan="5">備註*字號者，為特教中心無庫存之意。</td>
			<?php
			if (isset($_SESSION['account'])) {
			?>
				<td></td>
			<?php
			}
			?>
		</tr>
	</table>

	<?php
	if (isset($_SESSION['account'])) {
	?>
		<div class="modfix"><a href="?page=addcollection">新增書籍</a></div>
	<?php
	}
	?>
</div>

<script>
function changeNumber(id, oldSort){
	var newSort = prompt("新的編號是?", oldSort);
	window.location.href="../modcollectionorder.php?id="+id+"&sort="+newSort;
}
</script>