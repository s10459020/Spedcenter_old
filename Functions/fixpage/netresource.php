<h1>網路資源</h1>
<div class="mainBlockContent">
	<!--<h3>校內行政資源</h3>
			<ul>
				<li><a href="http://web.nutn.edu.tw/gac200/">教務處</a></li>
				<li><a href="http://web.nutn.edu.tw/gac400/">總務處</a></li>
				<li><a href="http://web.nutn.edu.tw/gac300/">學生事務處</a></li>
				<li><a href="http://web.nutn.edu.tw/gac690/">輔導中心</a></li>
				<li><a href="http://web.nutn.edu.tw/gac370/newweb/">教官室</a></li>
				<li><a href="http://web.nutn.edu.tw/gac340/">體育室</a></li>
				<li><a href="http://lib.nutn.edu.tw">圖書館</a></li>
				<li><a href="http://web.nutn.edu.tw/gac640/">特殊教育學系</a></li>
				<li><a href="http://www2.nutn.edu.tw/vhc/">視障教育與重建中心</a></li>
				<li><a href="http://web.nutn.edu.tw/gac646/">特殊教育中心</a></li>
				<li><a href="http://web.nutn.edu.tw/cn632/">文化與自然資源學系</a></li>
				<li><a href="http://web.nutn.edu.tw/gac680/">幼兒教育學系</a></li>
				<li><a href="http://web.nutn.edu.tw/bst/">生物科技學系</a></li>
				<li><a href="http://web.nutn.edu.tw/management/">行政管理學系</a></li>
				<li><a href="http://140.133.13.40/">資訊工程學系</a></li>
				<li><a href="http://web.nutn.edu.tw/gac610/">教育學系</a></li>
			</ul>-->
	<h3>資源連結</h3>
	<ul>
		<li><a href="http://crpd.sfaa.gov.tw">身心障礙者權利公約資訊網</a></li>
	</ul>
	<h3>各區特教中心</h3>
	<ul>
		<li><a href="http://web.spc.ntnu.edu.tw/main.php">臺灣師範大學特殊教育中心</a></li>
		<li><a href="http://speccen.utaipei.edu.tw/bin/home.php">臺北市立大學特教中心</a></li>
		<li><a href="http://r2.ntue.edu.tw/">臺北教育大學特殊教育中心</a></li>
		<li><a href="http://sec.cycu.edu.tw/wSite/mp?mp=19">中原大學特殊教育中心</a></li>
		<li><a href="http://www.nhcue.edu.tw/~spec/">清華大學特殊教育中心</a></li>
		<li><a href="http://spc.ntcu.edu.tw/index.php?ItemTag=1">臺中教育大學特殊教育中心</a></li>
		<li><a href="http://spc.ncue.edu.tw/bin/home.php">彰化師範大學特殊教育中心</a></li>
		<li><a href="http://www.ncyu.edu.tw/spedc/">嘉義大學特殊教育中心</a></li>
		<li><a href="http://ksped.nknu.edu.tw/Default.aspx">高雄師範大學特殊教育中心</a></li>
		<li><a href="http://spec.nptu.edu.tw/bin/home.php">屏東大學特殊教育中心</a></li>
		<li><a href="http://sect.nttu.edu.tw/bin/home.php">臺東大學特殊教育中心</a></li>
		<li><a href="http://www.cse.ndhu.edu.tw/bin/home.php">東華大學特殊教育中心</a></li>
	</ul>
</div>