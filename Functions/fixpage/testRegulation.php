<h1>測驗申請</h1>
<div class="mainBlockContent">
	<h3>特教中心測驗工具借閱需知</h3>
	<?php
	if (isset($_SESSION['account'])) {
	?>
		<div class="modfix"><a href="?modfix=8">編輯</a></div>
	<?php
	}
	?>
	<ol>
		<?php
		foreach ($notes as $note) {
		?>
			<li>
				<?php
				echo $note
				?>
			</li>
		<?php
		}
		?>
	</ol>

	<?php
	$fileRows = mysqli_fetch_assoc($fileResult);
	$filename = explode(';', $fileRows['Content'])[0];
	if (file_exists("../uploads/testregulation/" . $filename)) {
	?>
		<div class="fileBlock">
			<a href="../uploads/testregulation/<?php echo $filename ?>" download>
				<span class="downloadBtn" alt="下載檔案-測驗申請表.pdf">測驗申請表下載</span>
			</a>
		</div>
	<?php
	}
	?>
</div>