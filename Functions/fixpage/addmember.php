<h1>新增成員資料</h1>
<div class="addNewsForm">
	<form action="../addmember.php" method="post" enctype="multipart/form-data">
		<div class="controls">
			<label for="Name">姓名</label>
			<input name="Name" type="text" placeholder="請輸入姓名" required />
		</div>
		<div class="controls">
			<label for="Info">成員資訊</label>
			<textarea name="Info" placeholder="請輸入成員資訊" wrap="virtual" draggable="false" required></textarea>
		</div>
		<div class="controls">
			<label for="Email">Email</label>
			<input name="Email" type="text" placeholder="請輸入Email" required />
		</div>
		<div class="controls">
			<label for="Upload">個人照片</label>
			<br />
			<input name="Upload" type="file" />
		</div>
		<div class="controls">
			<input type="submit" value="送出"></input>
			<a href="index.php">
				<span class="Cancel">取消</span>
			</a>
		</div>
	</form>
</div>