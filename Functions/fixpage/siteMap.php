<h1>網站導覽</h1>
<label class="accessinfo">快捷鍵:Alt+N 跳到瀏覽列、Alt+M 跳到主要內容區域</label>
<div class="mainBlockContent">
	<map title="網站導覽">
		<a href="?page=introduction">
			<span class="guideItem">
				<label>中心簡介</label>
			</span>
		</a>
		<a href="?page=outline">
			<span class="guideItem">
				<label>業務概況</label>
			</span>
		</a>
		<a href="?page=consultation">
			<span class="guideItem">
				<label>諮詢服務</label>
			</span>
		</a>
		<a href="?page=collection">
			<span class="guideItem">
				<label>中心出版品</label>
			</span>
		</a>
		<a href="?page=testRegulation">
			<span class="guideItem">
				<label>測驗申請</label>
			</span>
		</a>
		<a href="?activities=1">
			<span class="guideItem">
				<label>研習活動</label>
			</span>
		</a>
		<a href="mailto:gloria@mail.nutn.edu.tw">
			<span class="guideItem">
				<label>意見信箱</label>
			</span>
		</a>
		<a href="http://campus.nutn.edu.tw/netForum/default.aspx">
			<span class="guideItem">
				<label>留言板</label>
			</span>
		</a>
	</map>
</div>