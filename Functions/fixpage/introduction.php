<h1>中心簡介</h1>
<div class="mainBlockContent introductionBlock">
	<h3>沿革</h3>
	<?php
	if (isset($_SESSION['account'])) {
	?>
		<div class="modfix"><a href="?modfix=1">編輯</a></div>
	<?php
	}
	?>
	<p>
		<?php
		echo nl2br(mysqli_fetch_assoc($result)['Content'])
		?>
	</p>
	<h3>組織</h3>
	<?php
	if (isset($_SESSION['account'])) {
	?>
		<div class="modfix"><a href="?modfix=2">編輯</a></div>
	<?php
	}
	?>
	<p>
		<?php
		echo nl2br(mysqli_fetch_assoc($result)['Content'])
		?>
	</p>
	<h3>設備</h3>
	<?php
	if (isset($_SESSION['account'])) {
	?>
		<div class="modfix"><a href="?modfix=3">編輯</a></div>
	<?php
	}
	?>
	<p>
		<?php
		echo nl2br(mysqli_fetch_assoc($result)['Content'])
		?>
	</p>
	<h3>人員</h3>
	<table>
		<?php
		$result = mysqli_query($connection, "SELECT * FROM CSE_Members ORDER BY No");
		while ($row = mysqli_fetch_assoc($result)) {
		?>
			<tr>
				<td>
					<img src="../img/<?php echo $row['ImageFileName'] ?>" alt="<?php echo $row['Name'] ?> 照片" />
				</td>
				<td>
					<ul>
						<?php
						if (isset($_SESSION['account'])) {
						?>
							<div class="modfix"><a href="?modmember=<?php echo $row['No'] ?>">編輯</a> <a href="?msg=delmember&delmemberno=<?php echo $row['No'] ?>">刪除</a></div>
						<?php
						}
						foreach (explode("\r\n", $row['Info']) as $line) {
						?>
							<li><?php echo $line ?></li>
						<?php
						}
						?>
						<li>電子信箱：<a href="mailto:<?php echo $row['Email'] ?>"><?php echo  $row['Email'] ?></a></li>
					</ul>
				</td>
			</tr>
		<?php
		}
		?>
	</table>
	<?php
	if (isset($_SESSION['account'])) {
	?>
		<div class="modfix"><a href="?page=addmember">新增</a></div>
	<?php
	}
	?>
</div>