<h1>新增叢書資料</h1>
<div class="addNewsForm">
	<form action="../addcollection.php" method="post" enctype="multipart/form-data">
		<div class="controls">
			<label for="Class">類別</label>
			<input name="Class" type="text" placeholder="請輸入類別" />
		</div>
		<div class="controls">
			<label for="Author">作者</label>
			<input name="Author" type="text" placeholder="請輸入作者" />
		</div>
		<div class="controls">
			<label for="BookName">書名</label>
			<input name="BookName" type="text" placeholder="請輸入書名" required />
		</div>
		<div class="controls">
			<label for="Remarks">備註</label>
			<input name="Remarks" type="text" placeholder="請輸入備註" />
		</div>
		<div class="controls">
			<label for="Upload">檔案</label>
			<br />
			<input name="Upload" type="file" />
		</div>
		<div class="controls">
			<input type="submit" value="送出"></input>
			<a href="index.php">
				<span class="Cancel">取消</span>
			</a>
		</div>
	</form>
</div>