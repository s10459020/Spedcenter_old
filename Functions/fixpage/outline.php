<h1>業務概況</h1>
<div class="mainBlockContent">
	<h3>服務項目</h3>
	<?php
	if (isset($_SESSION['account'])) {
	?>
		<div class="modfix text-lg"><a href="?modfix=4">編輯</a></div>
	<?php
	}
	?>
	<p class="text-lg">
		<?php echo nl2br(mysqli_fetch_assoc($result)['Content']) ?>
	</p>
	<h3>服務對象</h3>
	<?php
	if (isset($_SESSION['account'])) {
	?>
		<div class="modfix"><a href="?modfix=5">編輯</a></div>
	<?php
	}
	?>
	<p class="text-lg">
		<?php echo nl2br(mysqli_fetch_assoc($result)['Content']) ?>
	</p>
	<h3>諮詢人員</h3>
	<?php
	if (isset($_SESSION['account'])) {
	?>
		<div class="modfix"><a href="?modfix=6">編輯</a></div>
	<?php
	}
	?>
	<p class="text-lg">
		<?php echo nl2br(mysqli_fetch_assoc($result)['Content']) ?>
	</p>
</div>