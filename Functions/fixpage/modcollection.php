<?php
	$result = mysqli_query($connection, "SELECT * FROM CSE_Collection WHERE No=$_GET[no]");
	if( mysqli_num_rows($result) == 0 ) {
		header("Location: index.php?msg=nodata");
		exit();
	}
	
	$row = mysqli_fetch_assoc($result);
?>
<h1>修改叢書資料</h1>
<div class="addNewsForm">
	<form action="../modcollection.php" method="post" enctype="multipart/form-data">
		<div class="controls">
			<input name="No" type="hidden" value="<?php echo $row['No'] ?>"/>
		</div>
		<div class="controls">
			<label for="Class">類別</label>
			<input name="Class" type="text" placeholder="請輸入類別" value="<?php echo $row['Class'] ?>"/>
		</div>
		<div class="controls">
			<label for="Author">作者</label>
			<input name="Author" type="text" placeholder="請輸入作者" value="<?php echo $row['Author'] ?>" />
		</div>
		<div class="controls">
			<label for="BookName">書名</label>
			<input name="BookName" type="text" placeholder="請輸入書名" value="<?php echo $row['BookName'] ?>" required />
		</div>
		<div class="controls">
			<label for="Remarks">備註</label>
			<input name="Remarks" type="text" placeholder="請輸入備註" value="<?php echo $row['Remarks'] ?>" />
		</div>
		<div class="controls">
			<label for="Upload">檔案</label>
			<br />
			<input name="Upload" type="file" value="index.php" onchange="console.dir(this)">
			../collection_appends/<?php echo $row['File'] ?>
		</div>
		<div class="controls">
			<input type="submit" value="送出"></input>
			<a href="index.php">
				<span class="Cancel">取消</span>
			</a>
		</div>
	</form>
</div>