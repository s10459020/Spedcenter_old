<?php 
	$classResult = mysqli_query($connection, "SELECT * FROM CSE_Collection GROUP BY Class");
	$classNum = mysqli_num_rows($classResult);
?>
<h1>叢書</h1>
<div class="mainBlockContent">
	<map>
		<a href="?page=collection">
			<span class="guideItem">
				<label>所有書籍</label>
			</span>
		</a>
		<?php
		while ($row = mysqli_fetch_assoc($classResult)) {
			echo 
			"<a href='?page=collection&class=$row[Class]'>
				<span class='guideItem'>
					<label>$row[Class]</label>
				</span>
			</a>";
		}
		?>
	</map>
</div>	