<h1>新增公告</h1>
<div class="addNewsForm">
	<form action="../addnews.php" method="post" enctype="multipart/form-data">
		<div class="controls">
			<label for="Title">標題</label>
			<input name="Title" type="text" placeholder="請輸入標題" required />
		</div>
		<div class="controls">
			<label for="Date">日期</label>
			<input name="Date" type="date" required />
		</div>
		<div class="controls">
			<label for="Study">研習活動</label>
			<input id="Study" name="Study" type="checkbox" />
		</div>
		<div class="controls">
			<input name="Upload[]" type="file" multiple />
		</div>
		<div class="controls">
			<input id="link" type="text" placeholder="輸入欲插入的超連結網址" /><input type="button" onClick="insertLink()" value="插入超連結">
		</div>
		<script>
			function insertLink() {
				var textbox = document.getElementById("textbox");
				var link = document.getElementById("link");
				var l = link.value;
				textbox.value = textbox.value.substring(0, textbox.selectionEnd) + "<a href=\"" + l + "\">" + l + "</a>" + textbox.value.substring(textbox.selectionEnd, textbox.value.length);
				link.value = "";
				textbox.focus();
			}
		</script>
		<div class="controls">
			<textarea id="textbox" name="Content" placeholder="請輸入公告內文" wrap="virtual" draggable="false"></textarea>
		</div>
		<div class="controls">
			<input type="submit" value="送出"></input>
			<a href="index.php">
				<span class="Cancel">取消</span>
			</a>
		</div>
	</form>
</div>