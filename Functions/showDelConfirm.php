<?php

/**
 * 刪除公告警訊
 */
function showDelConfirm($delnewsno)
{
	if (is_numeric($delnewsno)) {
		$delnewsno = addslashes($_GET['delnewsno']);
	?>
		<div class="messageBox">
			<label id="msgBoxTitle">刪除公告</label><br>
			<label>確定要刪除這則公告嗎?</label><br>
			<label>(按下確定後將無法復原!)</label><br>
			<a href="index.php" class="cancelBtn">取消</a>
			<a href="../delnews.php?delnewsno=<?php echo $delnewsno ?>" class="cancelBtn">確定</a>
		</div>
		<?php
	} else {
		showMsgBox('nodata');
	}
}

?>