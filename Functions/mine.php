<?php
function mime_content_type($str) {	
	if(substr($str, -3, 3) == "jpg")
		return "image/jpg";
	if(substr($str, -3, 3) == "png")
		return "image/png";
	if(substr($str, -3, 3) == "gif")
		return "image/gif";
	return "";
}
?>
