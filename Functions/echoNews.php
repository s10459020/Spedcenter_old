<?php

function echoNews($connection, $NewsNo)
{
	$result = mysqli_query($connection, "SELECT * FROM CSE_News WHERE No='" . $NewsNo . "'");
	if (mysqli_num_rows($result) == 0) {
		?>
		<div class="mainBlockContent">
			<div class="informationBlock">
				<h1 class="newsPageTitle">查無此公告</h1>
			</div>
		</div>
	<?php
	} else {
		$rows = mysqli_fetch_assoc($result);
	?>
		<div class="mainBlockContent">
			<div class="informationBlock">
				<h1 class="newsPageTitle"><?php echo $rows['Title'] ?></h1>
				
				<?php
				if ($rows['Type'] == '1') {
					echo "<h3 class='newsPageType'>研習活動</h3>";
				}
				if (isset($_SESSION['account'])) {
					echo "<h4><a class='delNewsA' href='?msg=delnews&delnewsno=$NewsNo'>刪除此貼文</a></h4>";
					echo "<h4><a class='delNewsA' href='?modnewsno=$NewsNo'>修改此貼文</a></h4>";
				}
				?>
				
				<h4><label class="newsPageDate">日期:<?php echo $rows['Date'] ?></label></h4>
			</div>
			
			<?php
			if ($rows['File'] != "") {
			?>
				<div class="newsPageImg">
					<?php
					foreach (explode(';', $rows['File']) as $filename) {
						$title = $rows['Title'];
						if (file_exists("../uploads/" . $filename) and substr(mime_content_type("../uploads/" . $filename), 0, 5) == "image" ) {
							echo "<img src='../uploads/$filename' alt=' $title (附圖)-$filename' />";
						}
					}
					?>
				</div>
				<?php
				foreach (explode(';', $rows['File']) as $filename) {
					if ($filename != "") {
				?>
						<div class="fileBlock">
							<a href="../uploads/<?php echo $filename ?>" download>
								<span class="downloadBtn" alt="<?php echo $filename ?>">下載檔案-<?php echo $filename ?></span>
							</a>
						</div>
			<?php
					}
				}
			}
			?>
			<p class="newsPageContent"><?php echo nl2br($rows['Content']) ?></p>
		</div>
<?php
	}
}

?>