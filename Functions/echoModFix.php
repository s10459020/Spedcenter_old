<?php

/**
 * 固定頁面修改
 */
function echoModFix($modfixNo, $connection)
{
	$result = mysqli_query($connection, 'SELECT * FROM CSE_Fix_Contents WHERE No=\'' . $modfixNo . '\'');
	if (mysqli_num_rows($result) == 1) {
		$row = mysqli_fetch_assoc($result);
	?>
		<h1>修改頁面-<?php echo $row['Name'] ?></h1>
		<?php
		if ($modfixNo == 7) {
		?>
			<div class="addNewsForm">
				<form action="../modconsultation.php" method="post" enctype="multipart/form-data">
					<div class="controls">
						<input name="Upload[]" type="file" accept="image/png, image/jpeg" multiple />
					</div>
					<div class="controls">
						<input type="submit" value="送出"></input>
						<a href="index.php">
							<span class="Cancel">取消</span>
						</a>
					</div>
				</form>
			</div>
		<?php
		} elseif ($modfixNo == 8) {
			$noteResult = mysqli_query($connection, 'SELECT * FROM CSE_Fix_Contents WHERE No=9');
			$noteRow = mysqli_fetch_assoc($noteResult);
		?>

			<div class="addNewsForm">
				<form action="../modtestregulation.php" method="post" enctype="multipart/form-data">
					<div class="controls">
						<label for="Note">特教中心測驗工具借閱需知</label>
						<textarea name="Note" placeholder="請輸入借閱需知" wrap="virtual" draggable="false" required>
							<?php echo $noteRow["Content"] ?>
						</textarea>
					</div>
					<div class="controls">
						<input name="Upload[]" type="file" required />
					</div>
					<div class="controls">
						<input type="submit" value="送出"></input>
						<a href="index.php">
							<span class="Cancel">取消</span>
						</a>
					</div>
				</form>
			</div>
		<?php
		} else {
		?>
			<div class="addNewsForm">
				<form action="../modfix.php" method="post">
					<div class="controls">
						<textarea name="Content" placeholder="請輸入欲修改成甚麼內容" wrap="virtual" draggable="false"><?php echo $row['Content'] ?></textarea>
					</div>
					<input name="ModFixNo" type="hidden" value="<?php echo $modfixNo ?>" />
					<div class="controls">
						<input type="submit" value="送出"></input>
						<a href="index.php">
							<span class="Cancel">取消</span>
						</a>
					</div>
				</form>
			</div>
		<?php
		}
	} else {
		header("Location: index.php?msg=nodata");
	}
}

?>