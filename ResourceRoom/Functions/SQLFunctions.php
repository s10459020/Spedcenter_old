<?php
function conncetSQLDB($DBname)
{
	$servIP = '127.0.0.1';
	$servID = 'gac646';
	$servPwd = 'U5694R74WT';
	$connection = mysqli_connect($servIP, $servID, $servPwd, $DBname);
	date_default_timezone_set("Asia/Taipei");
	if (mysqli_connect_errno($connection)) {
		showMsgBoxCustom("警告", "資料庫連接失敗 錯誤代碼:" . mysqli_connect_errno());
		exit();
	}
	mysqli_query($connection, "SET NAMES 'utf8'");
	return $connection;
}
function showMsgBoxCustom($title, $text)
{
	echo '<div class="messageBox">
			<label id="msgBoxTitle">' . $title . '</label><br>
			<label>' . $text . '</label><br>
			<a class="cancelBtn" href="index.php">確定</a>
		</div>';
}
function showDelConfirm($delnewsno)
{
	if (is_numeric($delnewsno)) {
		$delnewsno = addslashes($_GET['delnewsno']);
		echo '<div class="messageBox">
				  <label id="msgBoxTitle">刪除公告</label><br>
				  <label>確定要刪除這則公告嗎?</label><br>
				  <label>(按下確定後將無法復原!)</label><br>
				  <a href="index.php" class="cancelBtn">取消</a>
				  <a href="../delnews.php?delnewsno=' . $delnewsno . '" class="cancelBtn">確定</a>
			</div>';
	} else {
		showMsgBox('nodata');
	}
}
function showMsgBox($msg)
{
	switch ($msg) {
		case 'accountnotfound':
			showMsgBoxCustom("找不到此帳號", "請檢查您的帳號密碼，並再試一次");
			break;
		case 'loginsuccess':
			showMsgBoxCustom("登入成功", "您已登入主控台");
			break;
		case 'pswdnotcorrect':
			showMsgBoxCustom("登入錯誤", "您輸入的密碼有誤，請再試一次");
			break;
		case 'nodata':
			showMsgBoxCustom("輸入錯誤", "並未接收到資訊");
			break;
		case 'logout':
			showMsgBoxCustom("登出成功", "您現在已登出主控台");
			break;
		case 'accessdenied':
			showMsgBoxCustom("存取失敗", "請確認您已登入主控台");
			break;
		case 'addnewssuccess':
			showMsgBoxCustom("新增成功", "公告已成功上傳");
			break;
		case 'addnewssucuploadfailed':
			showMsgBoxCustom("新增失敗", "公告內容上傳成功，檔案上傳發生錯誤");
			break;
		case 'addnewstitlefailed':
			showMsgBoxCustom("新增失敗", "公告列表上傳時發生資料庫錯誤");
			break;
		case 'delnewssuccess':
			showMsgBoxCustom("刪除成功", "公告已成功刪除");
			break;
		case 'delnewscontentfailed':
			showMsgBoxCustom("刪除失敗", "公告內容移除時發生資料庫錯誤");
			break;
		case 'delnewstitlefailed':
			showMsgBoxCustom("刪除失敗", "公告列表移除時發生資料庫錯誤");
			break;
		case 'addnewscontentfailed':
			showMsgBoxCustom("新增失敗", "公告上傳時發生錯誤");
			break;
		case 'modfixsuccess':
			showMsgBoxCustom("修改成功", "頁面內容修改成功");
			break;
		case 'modfixfailed':
			showMsgBoxCustom("修改失敗", "頁面內容修改失敗");
			break;
		case 'addmembercontentfailed':
			showMsgBoxCustom("新增失敗", "成員資料上傳時發生錯誤");
			break;
		case 'addmembersuccess':
			showMsgBoxCustom("新增成功", "成員資料已成功上傳");
			break;
		case 'addmembersucuploadfailed':
			showMsgBoxCustom("新增失敗", "成員資料上傳成功，檔案上傳發生錯誤");
			break;
		case 'delmemberfailed':
			showMsgBoxCustom("刪除失敗", "成員資料移除時發生錯誤錯誤");
			break;
		case 'delmembersuccess':
			showMsgBoxCustom("刪除成功", "成員資料已成功刪除");
			break;
		case 'addmembersucSameFile':
			showMsgBoxCustom("修改成功", "圖片名稱重複，將使用原名稱圖片，若要更換新圖片請更改檔名");
			break;
		case 'delformsuccess':
			showMsgBoxCustom("刪除成功", "表格刪除成功");
			break;
		case 'delformfailed':
			showMsgBoxCustom("刪除失敗", "表格刪除失敗");
			break;
		case 'addformsuccess':
			showMsgBoxCustom("新增成功", "表格新增成功");
			break;
		case 'addformfailed':
			showMsgBoxCustom("新增失敗", "表格新增失敗");
			break;
		case 'addlawsuccess':
			showMsgBoxCustom("新增成功", "相關法規新增成功");
			break;
		case 'addlawfailed':
			showMsgBoxCustom("新增失敗", "相關法規新增失敗");
			break;
		case 'dellawsuccess':
			showMsgBoxCustom("刪除成功", "相關法規刪除成功");
			break;
		case 'dellawfailed':
			showMsgBoxCustom("刪除失敗", "相關法規刪除失敗");
			break;
		case 'addlinksuccess':
			showMsgBoxCustom("新增成功", "連結新增成功");
			break;
		case 'addlinkfailed':
			showMsgBoxCustom("新增失敗", "連結新增失敗");
			break;
		case 'dellinksuccess':
			showMsgBoxCustom("刪除成功", "連結刪除成功");
			break;
		case 'dellinkfailed':
			showMsgBoxCustom("刪除失敗", "連結刪除失敗");
			break;
		default:
			showMsgBoxCustom("custom", $msg);
	}
}
function echoFixPage($type, $connection)
{
	switch ($type) {
		case 'addNews':
			echo '<h1>新增公告</h1>
				<div class="addNewsForm">
					<form action="../addnews.php" method="post" enctype="multipart/form-data">
						<div class="controls">
							<label for="Title">標題</label>
							<input name="Title" type="text" placeholder="請輸入標題" required/>
						</div>
						<div class="controls">
							<label for="Date">日期</label>
							<input name="Date" type="date" required/>
						</div>
						<div class="controls">
							<label for="Study">研習活動</label>
							<input id="Study" name="Study" type="checkbox"/>
						</div>
						<div class="controls">
							<input name="Upload[]" type="file" multiple/>
						</div>
						<div class="controls">
							<input id="link" type="text" placeholder="輸入欲插入的超連結網址"/><input type="button" onClick="insertLink()" value="插入超連結">
						</div>
						<script>
							function insertLink(){
								var textbox = document.getElementById("textbox");
								var link = document.getElementById("link");
								var l = link.value;
								textbox.value = textbox.value.substring(0,textbox.selectionEnd) + "<a href=\""+l+"\">" + l + "</a>" + textbox.value.substring(textbox.selectionEnd,textbox.value.length);
								link.value = "";
								textbox.focus();
							}
						</script>
						<div class="controls">
							<textarea id="textbox" name="Content" placeholder="請輸入公告內文" wrap="virtual" draggable="false"></textarea>
						</div>
						<div class="controls">
							<input type="submit" value="送出"></input>
							<a href="index.php">
								<span class="Cancel">取消</span>
							</a>
						</div>
					</form>
				</div>';
			break;
		case 'addmember':
			echo '<h1>新增成員資料</h1>
					<div class="addNewsForm">
						<form action="../addmember.php" method="post" enctype="multipart/form-data">
							<div class="controls">
									<label for="Name">姓名</label>
									<input name="Name" type="text" placeholder="請輸入姓名" required/>
							</div>
							<div class="controls">
								<label for="Cate">職位</label>
								<select name="Cate">
									<option value="0" >中心主任</option>
									<option value="1" >系主任</option>
									<option value="2" >各類輔導教授-視覺障礙類</option>
									<option value="4" >各類輔導教授-聽覺障礙類</option>
									<option value="3" >各類輔導教授-肢體障礙類</option>
									<option value="5" >各類輔導教授-情緒障礙類</option>
									<option value="11" >各類輔導教授-學習障礙類</option>
									<option value="6" >各類輔導教授-身體病弱類</option>
									<option value="7" >資源教室輔導員</option>
									<option value="8" >人力資源-特殊教育中心</option>
									<!--
									<option value="9" >人力資源-特殊教育學系</option>
									<option value="10" >人力資源-視障教育與重建中心</option>
									-->
								</select>
							</div>
							<div class="controls">
								<label for="Info">成員資訊</label>
								<textarea name="Info" placeholder="請輸入成員資訊" wrap="virtual" draggable="false" required></textarea>
							</div>
							<div class="controls">
									<label for="Email">Email</label>
									<input name="Email" type="text" placeholder="請輸入Email" required/>
							</div>
							<div class="controls">
								<label for="Upload">個人照片</label>
								<br/>
								<input name="Upload" type="file"/>
							</div>
							<div class="controls">
								<input type="submit" value="送出"></input>
								<a href="index.php">
										<span class="Cancel">取消</span>
								</a>
							</div>
						</form>
					</div>';
			break;
		case 'addform':
			echo '<h1>新增下載表單</h1>
				<div class="addNewsForm">
					<form action="../addform.php" method="post" enctype="multipart/form-data">
						<div class="controls">
							<label for="Name">檔案名稱</label>
							<input name="Name" type="text" required/>
						</div>
						<h3>請上傳表單的檔案</h3>
						<div class="controls">
							<input name="File" type="file" required/>
						</div>
						<div class="controls">
							<input type="submit" value="送出"></input>
							<a href="index.php">
									<span class="Cancel">取消</span>
							</a>
						</div>
					</form>
				</div>';
			break;
		case 'addlaw':
			echo '<h1>新增相關法規</h1>
				<div class="addNewsForm">
					<form action="../addlaw.php" method="post" enctype="multipart/form-data">
						<div class="controls">
							<label for="Name">相關法規檔案名稱</label>
							<input name="Name" type="text" required/>
						</div>
						<h3>請上傳表單的檔案</h3>
						<div class="controls">
							<input name="File" type="file" required/>
						</div>
						<div class="controls">
							<input type="submit" value="送出"></input>
							<a href="index.php">
									<span class="Cancel">取消</span>
							</a>
						</div>
					</form>
				</div>';
			break;
		case 'addlink':
			echo '<h1>新增連結</h1>
				<div class="addNewsForm">
					<form action="../addlink.php" method="post">
						<div class="controls">
							<label for="Name">連結名稱</label>
							<input name="Name" type="text" required/>
						</div>
						<div class="controls">
							<label for="Cate">連結分類</label>
							<select name="Cate">
							<option value="1" >校內行政資源</option>
								<option value="2" >各區特教中心</option>
								<option value="3" >校外好站連結</option>
							</select>
						</div>
						<div class="controls">
							<label for="HyperLink">連結網址</label>
							<input name="HyperLink" type="text" required/>
						</div>
						<input name="ModLinkNo" type="hidden" />
						<div class="controls">
							<input type="submit" value="送出"></input>
							<a href="index.php">
									<span class="Cancel">取消</span>
							</a>
						</div>
					</form>
				</div>';
			break;
		case 'siteMap':
			echo '<h1>網站導覽</h1>
					<label class="accessinfo">快捷鍵:Alt+N 跳到瀏覽列、Alt+M 跳到主要內容區域</label>
					<div class="mainBlockContent">
					<map title="網站導覽">
						<a href="?page=introduction">
							<span class="guideItem">
								<label>教室簡介</label>
							</span>
						</a>
						<a href="?page=members">
							<span class="guideItem">
								<label>成員執掌</label>
							</span>
						</a>
						<a href="?page=manager">
							<span class="guideItem">
								<label>管理運作</label>
							</span>
						</a>
						<a href="?page=services">
							<span class="guideItem">
								<label>各項服務</label>
							</span>
						</a>
						<a href="?page=current">
							<span class="guideItem">
								<label>關於現況</label>
							</span>
						</a>
						<a href="?page=downloads">
							<span class="guideItem">
								<label>各類表格</label>
							</span>
						</a>
						<a href="?page=laws">
							<span class="guideItem">
								<label>相關法規</label>
							</span>
						</a>
						<a href="?page=links">
							<span class="guideItem">
								<label>網路資源</label>
							</span>
						</a>
						<a href="mailto:iamcici@mail.nutn.edu.tw">
							<span class="guideItem">
								<label>意見信箱</label>
							</span>
						</a>
					</map>
					</div>';
			break;
		case 'aboutResourceRoom':
			echo '<h1>關於資源教室</h1>
					<div class="mainBlockContent">
					<map>
						<a href="?page=introduction">
							<span class="guideItem">
								<label>教室簡介</label>
							</span>
						</a>
						<a href="?page=members">
							<span class="guideItem">
								<label>成員執掌</label>
							</span>
						</a>
						<a href="?page=manager">
							<span class="guideItem">
								<label>管理運作</label>
							</span>
						</a>
					</map>
					</div>';
			break;
		case 'services':
			$result = mysqli_query($connection, "SELECT * FROM CSE_Fix_Contents_RR WHERE No>=3 AND No<=9 ORDER BY No");
			$row1 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			$row2 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			$row3 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			$row4 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			$row5 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			$row6 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			$row7 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			if (isset($_SESSION['account'])) {
				$row1 = '<div class="modfix"><a href="?modfix=3">編輯</a></div>' . $row1;
				$row2 = '<div class="modfix"><a href="?modfix=4">編輯</a></div>' . $row2;
				$row3 = '<div class="modfix"><a href="?modfix=5">編輯</a></div>' . $row3;
				$row4 = '<div class="modfix"><a href="?modfix=6">編輯</a></div>' . $row4;
				$row5 = '<div class="modfix"><a href="?modfix=7">編輯</a></div>' . $row5;
				$row6 = '<div class="modfix"><a href="?modfix=8">編輯</a></div>' . $row6;
				$row7 = '<div class="modfix"><a href="?modfix=9">編輯</a></div>' . $row7;
			}
			echo '<h1>各項服務</h1>
					<div class="mainBlockContent">
						<h3>生活輔導</h3>
						' . $row1 . '</p>
						<h3>學業輔導</h3>
						' . $row2 . '</p>
						<h3>社會適應</h3>
						' . $row3 . '</p>
						<h3>轉銜服務</h3>
						' . $row4 . '</p>
						<h3>心理服務</h3>
						' . $row5 . '</p>
						<h3>輔具借用與申請</h3>
						' . $row6 . '</p>
						<h3>特教宣導</h3>
						' . $row7 . '</p>
					</div>';
			/*echo '<h1>各項服務</h1>
					<div class="mainBlockContent">
						<a href="?page=service_1">
							<span class="guideItem">
								<label>生活輔導</label>
							</span>
						</a>
						<a href="?page=service_2">
							<span class="guideItem">
								<label>學業輔導</label>
							</span>
						</a>
						<a href="?page=service_3">
							<span class="guideItem">
								<label>社會適應</label>
							</span>
						</a>
						<a href="?page=service_4">
							<span class="guideItem">
								<label>轉銜服務</label>
							</span>
						</a>
						<a href="?page=service_5">
							<span class="guideItem">
								<label>心理服務</label>
							</span>
						</a>
						<a href="?page=service_6">
							<span class="guideItem">
								<label>輔具借用與申請</label>
							</span>
						</a>
						<a href="?page=service_7">
							<span class="guideItem">
								<label>特教宣傳</label>
							</span>
						</a>
					</div>';*/
			break;
		case 'current':
			$result = mysqli_query($connection, "SELECT * FROM CSE_Fix_Contents_RR WHERE No>=10 AND No<=16 ORDER BY No");
			$row1 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			$row2 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			$row3 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			$row4 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			$row5 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			$row6 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			$row7 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			if (isset($_SESSION['account'])) {
				$row1 = '<div class="modfix"><a href="?modfix=10">編輯</a></div>' . $row1;
				$row2 = '<div class="modfix"><a href="?modfix=11">編輯</a></div>' . $row2;
				$row3 = '<div class="modfix"><a href="?modfix=12">編輯</a></div>' . $row3;
				$row4 = '<div class="modfix"><a href="?modfix=13">編輯</a></div>' . $row4;
				$row5 = '<div class="modfix"><a href="?modfix=14">編輯</a></div>' . $row5;
				$row6 = '<div class="modfix"><a href="?modfix=15">編輯</a></div>' . $row6;
				$row7 = '<div class="modfix"><a href="?modfix=16">編輯</a></div>' . $row7;
			}
			echo '<h1>關於現況</h1>
					<div class="mainBlockContent">
						<h3>生活輔導</h3>
						' . $row1 . '</p>
						<h3>學業輔導</h3>
						' . $row2 . '</p>
						<h3>社會適應</h3>
						' . $row3 . '</p>
						<h3>轉銜服務</h3>
						' . $row4 . '</p>
						<h3>心理服務</h3>
						' . $row5 . '</p>
						<h3>輔具借用與申請</h3>
						' . $row6 . '</p>
						<h3>特教宣導</h3>
						' . $row7 . '</p>
					</div>';
			break;
		case 'downloads':
			echo '<h1>各類表格</h1>
					<div class="mainBlockContent">';
			$result = mysqli_query($connection, "SELECT * FROM CSE_Forms_RR ORDER BY No");
			while ($row = mysqli_fetch_assoc($result)) {
				$edit = "";
				if (isset($_SESSION['account'])) {
					$edit = '<div class="modfix"><a href="?modform=' . $row['No'] . '">編輯</a><a href="?msg=delform&delformno=' . $row['No'] . '">刪除</a></div>';
				}
				echo '
							<h3>' . $row['Name'] . '</h3>
							<ul>
								<li><a href="../uploads/' . $row['FileName'] . '" alt="' . $row['FileName'] . '" download>檔案下載</a>' . $edit . '</li>
							</ul>';
			}
			if (isset($_SESSION['account'])) {
				echo '<div class="modfix"><a href="?page=addform">新增</a></div>';
			}
			echo '</div>';
			break;
			/*echo '<h1>關於現況</h1>
					<div class="mainBlockContent">
						<a href="?page=service_1_c">
							<span class="guideItem">
								<label>生活輔導</label>
							</span>
						</a>
						<a href="?page=service_2_c">
							<span class="guideItem">
								<label>學業輔導</label>
							</span>
						</a>
						<a href="?page=service_3_c">
							<span class="guideItem">
								<label>社會適應</label>
							</span>
						</a>
						<a href="?page=service_4_c">
							<span class="guideItem">
								<label>轉銜服務</label>
							</span>
						</a>
						<a href="?page=service_5_c">
							<span class="guideItem">
								<label>心理服務</label>
							</span>
						</a>
						<a href="?page=service_6_c">
							<span class="guideItem">
								<label>輔具借用與申請</label>
							</span>
						</a>
						<a href="?page=service_7_c">
							<span class="guideItem">
								<label>特教宣傳</label>
							</span>
						</a>
					</div>';*/
			break;
		case 'links':
			echo '<h1>網路資源</h1>
					<div class="mainBlockContent">
						<h3>校內行政資源</h3>
						<ul>';
			$result = mysqli_query($connection, "SELECT * FROM CSE_Links_RR WHERE Cate = '1'");
			if (isset($_SESSION['account'])) {
				while ($row = mysqli_fetch_assoc($result)) {
					echo '<li><a href="' . $row['HyperLink'] . '">' . $row['Name'] . '</a>
								<div class="modfix"><a href="?modlink=' . $row['No'] . '">編輯</a></div>
								<div class="modfix"><a href="?msg=dellink&dellinkno=' . $row['No'] . '">刪除</a></div></li>';
				}
			} else {
				while ($row = mysqli_fetch_assoc($result)) {
					echo '<li><a href="' . $row['HyperLink'] . '">' . $row['Name'] . '</a></li>';
				}
			}
			echo '</ul>
						<h3>各區特教中心</h3>
						<ul>';
			$result = mysqli_query($connection, "SELECT * FROM CSE_Links_RR WHERE Cate = '2'");
			if (isset($_SESSION['account'])) {
				while ($row = mysqli_fetch_assoc($result)) {
					echo '<li><a href="' . $row['HyperLink'] . '">' . $row['Name'] . '</a>
								<div class="modfix"><a href="?modlink=' . $row['No'] . '">編輯</a></div>
								<div class="modfix"><a href="?msg=dellink&dellinkno=' . $row['No'] . '">刪除</a></div></li>';
				}
			} else {
				while ($row = mysqli_fetch_assoc($result)) {
					echo '<li><a href="' . $row['HyperLink'] . '">' . $row['Name'] . '</a></li>';
				}
			}
			echo '</ul>
						<h3>校外好站連結</h3>
						<ul>';
			$result = mysqli_query($connection, "SELECT * FROM CSE_Links_RR WHERE Cate = '3'");
			if (isset($_SESSION['account'])) {
				while ($row = mysqli_fetch_assoc($result)) {
					echo '<li><a href="' . $row['HyperLink'] . '">' . $row['Name'] . '</a>
								<div class="modfix"><a href="?modlink=' . $row['No'] . '">編輯</a></div>
								<div class="modfix"><a href="?msg=dellink&dellinkno=' . $row['No'] . '">刪除</a></div></li>';
				}
			} else {
				while ($row = mysqli_fetch_assoc($result)) {
					echo '<li><a href="' . $row['HyperLink'] . '">' . $row['Name'] . '</a></li>';
				}
			}
			echo '</ul>';

			if (isset($_SESSION['account'])) {
				echo '<div class="modfix"><a href="?page=addlaw">新增</a></div>';
			}
			echo '</div>';
			break;
		case 'laws':
			$result = mysqli_query($connection, "SELECT * FROM CSE_Laws_RR ORDER BY No");
			echo '<h1>相關法規</h1>
					<div class="mainBlockContent">';
			while ($row = mysqli_fetch_assoc($result)) {
				$edit = "";
				if (isset($_SESSION['account'])) {
					$edit = '<div class="modfix"><a href="?modlaw=' . $row['No'] . '">編輯</a><a href="?msg=dellaw&dellawno=' . $row['No'] . '">刪除</a></div>';
				}
				echo '
							<h3>' . $row['Name'] . '</h3>
							<ul>
								<li><a href="../uploads/' . $row['FileName'] . '" alt="' . $row['FileName'] . '" download>檔案下載</a>' . $edit . '</li>
							</ul>';
			}
			if (isset($_SESSION['account'])) {
				echo '<div class="modfix"><a href="?page=addlaw">新增</a></div>';
			}
			echo '</div>';
			echo '</div>';
			break;
		case 'others':
			echo '<h1>其他</h1>
					<div class="mainBlockContent">
					<map>
						<a href="?page=downloads">
							<span class="guideItem">
								<label>各類表格</label>
							</span>
						</a>
						<a href="?page=laws">
							<span class="guideItem">
								<label>相關法規</label>
							</span>
						</a>
						<a href="?page=links">
							<span class="guideItem">
								<label>網路資源</label>
							</span>
						</a>
						<a href="mailto:iamcici@mail.nutn.edu.tw">
							<span class="guideItem">
								<label>與我們聯絡</label>
							</span>
						</a>
					<map>
					</div>';
			break;
		case 'introduction':
			$result = mysqli_query($connection, "SELECT * FROM CSE_Fix_Contents_RR WHERE No>=1 AND No<=2 ORDER BY No");
			$row1 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			$row2 = '<p>' . nl2br(mysqli_fetch_assoc($result)['Content']);
			if (isset($_SESSION['account'])) {
				$row1 = '<div class="modfix"><a href="?modfix=1">編輯</a></div>' . $row1;
				$row2 = '<div class="modfix"><a href="?modfix=2">編輯</a></div>' . $row2;
			}
			echo '<h1>教室簡介</h1>
					<div class="mainBlockContent">
						<h2>歷史與緣由</h2>
						' . $row1 . '</p>
						<h2>設備</h2>
						' . $row2 . '</p>
		           	 </div>';
			break;
		case 'members':
			$result = mysqli_query($connection, "SELECT Content FROM CSE_Fix_Contents_RR WHERE No = '17'");
			$image = mysqli_fetch_assoc($result)['Content'];
			echo '<h1>成員執掌</h1>
					<div class="mainBlockContent introductionBlock">
						<h2>組織結構</h2>
						<img src="../uploads/' . $image . '" style="width:100%;max-width:600px;" alt="組織架構圖: 特教中心主任:(各類輔導教授:(視覺障礙類、肢體障礙類、聽覺障礙類、情緒障礙類)、資源教室工作人員、人力資源:(特殊教育中心、特殊教育學系、視障教育中心))"/>';
			if (isset($_SESSION['account'])) {
				echo '<div class="modfix"><a href="?modfix=17">編輯</a></div>';
			}
			echo '<h2>成員介紹</h2>
						<h3>中心主任</h3>
						<table>';
			$result = mysqli_query($connection, "SELECT * FROM CSE_Members_RR WHERE Cate = '0'");
			while ($row = mysqli_fetch_assoc($result)) {
				echo '<tr>
								<td>
									<img src="../img/' . $row['ImageFileName'] . '" alt="特教中心主任：' . $row['Name'] . ' 照片"/>
								</td>
								<td>
									<ul>';
				if (isset($_SESSION['account'])) {
					echo '<div class="modfix"><a href="?modmember=' . $row['No'] . '">編輯</a> <a href="?msg=delmember&delmemberno=' . $row['No'] . '">刪除</a></div>';
				}
				foreach (explode("\r\n", $row['Info']) as $line) {
					echo '<li>' . $line . '</li>';
				}
				echo '<li>電子信箱:<a href="mailto:' . $row['Email'] . '">' . $row['Email'] . '</a></li>
									</ul>
								</td>
							</tr>';
			}

			echo '</table>
				<h3>系主任</h3>
				<table>';
			$result = mysqli_query($connection, "SELECT * FROM CSE_Members_RR WHERE Cate = '1'");
			while ($row = mysqli_fetch_assoc($result)) {
				echo '<tr>
								<td>
									<img src="../img/' . $row['ImageFileName'] . '" alt="特教系主任：' . $row['Name'] . ' 照片"/>
								</td>
								<td>
									<ul>';
				if (isset($_SESSION['account'])) {
					echo '<div class="modfix"><a href="?modmember=' . $row['No'] . '">編輯</a> <a href="?msg=delmember&delmemberno=' . $row['No'] . '">刪除</a></div>';
				}
				foreach (explode("\r\n", $row['Info']) as $line) {
					echo '<li>' . $line . '</li>';
				}
				echo '<li>電子信箱:<a href="mailto:' . $row['Email'] . '">' . $row['Email'] . '</a></li>
									</ul>
								</td>
							</tr>';
			}
			echo '</table>
				<h3>各類輔導教授</h3>
				<h4>視覺障礙類</h4>
				<table>';
			$result = mysqli_query($connection, "SELECT * FROM CSE_Members_RR WHERE Cate = '2'");
			while ($row = mysqli_fetch_assoc($result)) {
				echo '<tr>
								<td>
									<img src="../img/' . $row['ImageFileName'] . '" alt="輔導老師：' . $row['Name'] . ' 照片"/>
								</td>
								<td>
									<ul>';
				if (isset($_SESSION['account'])) {
					echo '<div class="modfix"><a href="?modmember=' . $row['No'] . '">編輯</a> <a href="?msg=delmember&delmemberno=' . $row['No'] . '">刪除</a></div>';
				}
				foreach (explode("\r\n", $row['Info']) as $line) {
					echo '<li>' . $line . '</li>';
				}
				echo '<li>電子信箱:<a href="mailto:' . $row['Email'] . '">' . $row['Email'] . '</a></li>
									</ul>
								</td>
							</tr>';
			}
			echo '</table>
				<h4>聽覺障礙類</h4>
				<table>';
			$result = mysqli_query($connection, "SELECT * FROM CSE_Members_RR WHERE Cate = '4'");
			while ($row = mysqli_fetch_assoc($result)) {
				echo '<tr>
								<td>
									<img src="../img/' . $row['ImageFileName'] . '" alt="輔導老師：' . $row['Name'] . ' 照片"/>
								</td>
								<td>
									<ul>';
				if (isset($_SESSION['account'])) {
					echo '<div class="modfix"><a href="?modmember=' . $row['No'] . '">編輯</a> <a href="?msg=delmember&delmemberno=' . $row['No'] . '">刪除</a></div>';
				}
				foreach (explode("\r\n", $row['Info']) as $line) {
					echo '<li>' . $line . '</li>';
				}
				echo '<li>電子信箱:<a href="mailto:' . $row['Email'] . '">' . $row['Email'] . '</a></li>
									</ul>
								</td>
							</tr>';
			}
			echo '</table>
				<h4>肢體障礙類</h4>
				<table>';
			$result = mysqli_query($connection, "SELECT * FROM CSE_Members_RR WHERE Cate = '3'");
			while ($row = mysqli_fetch_assoc($result)) {
				echo '<tr>
								<td>
									<img src="../img/' . $row['ImageFileName'] . '" alt="輔導老師：' . $row['Name'] . ' 照片"/>
								</td>
								<td>
									<ul>';
				if (isset($_SESSION['account'])) {
					echo '<div class="modfix"><a href="?modmember=' . $row['No'] . '">編輯</a> <a href="?msg=delmember&delmemberno=' . $row['No'] . '">刪除</a></div>';
				}
				foreach (explode("\r\n", $row['Info']) as $line) {
					echo '<li>' . $line . '</li>';
				}
				echo '<li>電子信箱:<a href="mailto:' . $row['Email'] . '">' . $row['Email'] . '</a></li>
									</ul>
								</td>
							</tr>';
			}
			echo '</table>
				<h4>情緒障礙類</h4>
				<table>';
			$result = mysqli_query($connection, "SELECT * FROM CSE_Members_RR WHERE Cate = '5'");
			while ($row = mysqli_fetch_assoc($result)) {
				echo '<tr>
								<td>
									<img src="../img/' . $row['ImageFileName'] . '" alt="輔導老師：' . $row['Name'] . ' 照片"/>
								</td>
								<td>
									<ul>';
				if (isset($_SESSION['account'])) {
					echo '<div class="modfix"><a href="?modmember=' . $row['No'] . '">編輯</a> <a href="?msg=delmember&delmemberno=' . $row['No'] . '">刪除</a></div>';
				}
				foreach (explode("\r\n", $row['Info']) as $line) {
					echo '<li>' . $line . '</li>';
				}
				echo '<li>電子信箱:<a href="mailto:' . $row['Email'] . '">' . $row['Email'] . '</a></li>
									</ul>
								</td>
							</tr>';
			}
			echo '</table>
				<h4>學習障礙類</h4>
				<table>';
			$result = mysqli_query($connection, "SELECT * FROM CSE_Members_RR WHERE Cate = '11'");
			while ($row = mysqli_fetch_assoc($result)) {
				echo '<tr>
								<td>
									<img src="../img/' . $row['ImageFileName'] . '" alt="輔導老師：' . $row['Name'] . ' 照片"/>
								</td>
								<td>
									<ul>';
				if (isset($_SESSION['account'])) {
					echo '<div class="modfix"><a href="?modmember=' . $row['No'] . '">編輯</a> <a href="?msg=delmember&delmemberno=' . $row['No'] . '">刪除</a></div>';
				}
				foreach (explode("\r\n", $row['Info']) as $line) {
					echo '<li>' . $line . '</li>';
				}
				echo '<li>電子信箱:<a href="mailto:' . $row['Email'] . '">' . $row['Email'] . '</a></li>
									</ul>
								</td>
							</tr>';
			}
			echo '</table>
				<h4>身體病弱類</h4>
				<table>';
			$result = mysqli_query($connection, "SELECT * FROM CSE_Members_RR WHERE Cate = '6'");
			while ($row = mysqli_fetch_assoc($result)) {
				echo '<tr>
								<td>
									<img src="../img/' . $row['ImageFileName'] . '" alt="輔導老師：' . $row['Name'] . ' 照片"/>
								</td>
								<td>
									<ul>';
				if (isset($_SESSION['account'])) {
					echo '<div class="modfix"><a href="?modmember=' . $row['No'] . '">編輯</a> <a href="?msg=delmember&delmemberno=' . $row['No'] . '">刪除</a></div>';
				}
				foreach (explode("\r\n", $row['Info']) as $line) {
					echo '<li>' . $line . '</li>';
				}
				echo '<li>電子信箱:<a href="mailto:' . $row['Email'] . '">' . $row['Email'] . '</a></li>
									</ul>
								</td>
							</tr>';
			}
			echo '</table>
				<h3>資源教室輔導員</h3>
				<table>';
			$result = mysqli_query($connection, "SELECT * FROM CSE_Members_RR WHERE Cate = '7'");
			while ($row = mysqli_fetch_assoc($result)) {
				echo '<tr>
								<td>
									<img src="../img/' . $row['ImageFileName'] . '" alt="生活輔導員：' . $row['Name'] . ' 照片"/>
								</td>
								<td>
									<ul>';
				if (isset($_SESSION['account'])) {
					echo '<div class="modfix"><a href="?modmember=' . $row['No'] . '">編輯</a> <a href="?msg=delmember&delmemberno=' . $row['No'] . '">刪除</a></div>';
				}
				foreach (explode("\r\n", $row['Info']) as $line) {
					echo '<li>' . $line . '</li>';
				}
				echo '<li>電子信箱:<a href="mailto:' . $row['Email'] . '">' . $row['Email'] . '</a></li>
									</ul>
								</td>
							</tr>';
			}
			echo '</table>
				<h3>人力資源</h3>
				<h4>特殊教育中心</h4>
				<table>';
			$result = mysqli_query($connection, "SELECT * FROM CSE_Members_RR WHERE Cate = '8'");
			while ($row = mysqli_fetch_assoc($result)) {
				echo '<tr>
								<td>
									<img src="../img/' . $row['ImageFileName'] . '" alt="助理：' . $row['Name'] . ' 照片"/>
								</td>
								<td>
									<ul>';
				if (isset($_SESSION['account'])) {
					echo '<div class="modfix"><a href="?modmember=' . $row['No'] . '">編輯</a> <a href="?msg=delmember&delmemberno=' . $row['No'] . '">刪除</a></div>';
				}
				foreach (explode("\r\n", $row['Info']) as $line) {
					echo '<li>' . $line . '</li>';
				}
				echo '<li>電子信箱:<a href="mailto:' . $row['Email'] . '">' . $row['Email'] . '</a></li>
									</ul>
								</td>
							</tr>';
			}
			echo '</table>';
			/*
				echo '</table>
				<h4>特殊教育學系</h4>
				<table>';
				$result = mysqli_query($connection,"SELECT * FROM CSE_Members_RR WHERE Cate = '9'");
				while($row = mysqli_fetch_assoc($result)){
					echo '<tr>
								<td>
									<img src="../img/'.$row['ImageFileName'].'" alt="校務基金行政助理：'.$row['Name'].' 照片"/>
								</td>
								<td>
									<ul>';
										if(isset($_SESSION['account'])){
											echo '<div class="modfix"><a href="?modmember='.$row['No'].'">編輯</a> <a href="?msg=delmember&delmemberno='.$row['No'].'">刪除</a></div>';
										}
										foreach(explode("\r\n",$row['Info']) as $line){
											echo '<li>'.$line.'</li>';
										}
								echo '<li>電子信箱:<a href="mailto:'.$row['Email'].'">'.$row['Email'].'</a></li>
									</ul>
								</td>
							</tr>';
				}
				echo '</table>
				<h4>視障教育與重建中心</h4>
				<table>';
				$result = mysqli_query($connection,"SELECT * FROM CSE_Members_RR WHERE Cate = '10'");
				while($row = mysqli_fetch_assoc($result)){
					echo '<tr>
								<td>
									<img src="../img/'.$row['ImageFileName'].'" alt="助理：'.$row['Name'].' 照片"/>
								</td>
								<td>
									<ul>';
										if(isset($_SESSION['account'])){
											echo '<div class="modfix"><a href="?modmember='.$row['No'].'">編輯</a> <a href="?msg=delmember&delmemberno='.$row['No'].'">刪除</a></div>';
										}
										foreach(explode("\r\n",$row['Info']) as $line){
											echo '<li>'.$line.'</li>';
										}
								echo '<li>電子信箱:<a href="mailto:'.$row['Email'].'">'.$row['Email'].'</a></li>
									</ul>
								</td>
							</tr>';
				}
						echo '</table>';
				*/
			if (isset($_SESSION['account'])) {
				echo '<div class="modfix"><a href="?page=addmember">新增</a></div>';
			}
			echo '</div>';
			break;
		case 'manager':
			$result = mysqli_query($connection, "SELECT Content FROM CSE_Fix_Contents_RR WHERE No>17 AND No<22 ORDER BY No");
			echo '<h1>管理運作</h1>
					<div class="mainBlockContent">
						<h3>服務對象</h3>
						<p>';
			$imageInfo = mysqli_fetch_assoc($result)['Content'];
			$imageName = mysqli_fetch_assoc($result)['Content'];
			$tableInfo = mysqli_fetch_assoc($result)['Content'];
			$tableName = mysqli_fetch_assoc($result)['Content'];
			echo $imageInfo;
			if (isset($_SESSION['account'])) {
				echo '<div class="modfix"><a href="?modfix=18">編輯</a></div>';
			}
			echo '</p>
						<img style="width:100%; max-width:500px;" src="../uploads/' . $imageName . '" alt="' . $imageInfo . '"/>';
			if (isset($_SESSION['account'])) {
				echo '<div class="modfix"><a href="?modfix=19">編輯</a></div>';
			}
			echo '<h3>' . $tableInfo . '</h3>';
			if (isset($_SESSION['account'])) {
				echo '<div class="modfix"><a href="?modfix=20">編輯</a></div>';
			}
			echo '<img style="width:100%; max-width:500px;" src="../uploads/' . $tableName . '" alt="' . $tableInfo . '"/>';

			if (isset($_SESSION['account'])) {
				echo '<div class="modfix"><a href="?modfix=21">編輯</a></div>';
			}
			echo '</div>';
			break;
			/*case 'service_1':
				echo '<h1>各項服務</h1>
					<div class="mainBlockContent">
						<h3>生活輔導</h3>
						<ol>
							<li>無障礙空間：增進身障學生的無障礙生理與心理空間。</li>
							<li>個案會議：改善學生學習狀況及討論校園適應問題的解決辦法。</li>
							<li>訊息提供：促進學生參與跨校活動、民間社團活動，申請獎助學金等。</li>
							<li>資源提供：提供書籍、音樂、影片借閱及休憩活動場所。 </li>
							<li>建立身障生個案資料：建立身障生個案資料並隨時檢視追蹤，以期系統並有效地服務身障生。</li>
						</ol>
					</div>';
				break;*/
		case 'testRegulation':
			echo '<h1>測驗申請</h1>
					<div class="mainBlockContent">
					<h3>特教中心測驗工具借閱需知</h3>
					<ol>
					  <li> 校內單位僅限特教系師生借用，本系學生借閱測驗者，申請表需指導教授<span style="color:rgb(200,50,50);">簽名</span>。</li>
					  <li>若借閱之測驗工具為教師授課之用，請依借用規則，事先向特教中心辦理借用手續。 </li>
					  <li>特殊教育需求學生診斷用測驗工具，得由專業測驗人員以診斷使用為由方可借用。</li>
					  <li>校外單位（<span style="color:rgb(200,50,50);">限本校輔導區</span>）需依據<span style="color:rgb(200,50,50);">公文</span>，並填寫<span style="color:rgb(200,50,50);"">測驗申請書</span>始能借閱測驗工具。如欲借「魏氏智力測驗」，除公文外，尚需押<span style="color:rgb(200,50,50);">魏氏智力測驗研習證書(一張證書限借一份)</span>。</li>
					  <li>借閱期限為<span style="color:rgb(200,50,50);">2週</span>，逾期則需自行至特教中心辦理續借；續借以1次為限，續借期限以2週為限。</li>
					  <li>測驗出借前，借用人請先行檢查測驗內容，歸還時若有毀損、污損、遺失等，應由借用人負責，按測驗原價格<span style="color:rgb(200,50,50);">加倍賠償</span>，並<span style="color:rgb(200,50,50);"><strong>停止借用測驗六個月</strong></span>。</li>
					  <li>測驗工具之消耗品，如測驗紙、紀錄本、篩選紙等，本中心只提供借閱使用，不作為消費用途，請勿在消耗品上做記號。</li>
					  <li>歸還測驗時需由特教中心相關人員點收並簽名。</li>
					  <li>使用者對於評量工具之內容請<span style="color:rgb(200,50,50);"><strong>遵守測驗使用倫理</strong></span>應嚴加保密，絕對不可複製。</li>
					  <li>請填妥測驗借用申請表內相關資料。<br />
					</ol>
					<a href="../uploads/測驗申請表.pdf" download>測驗申請表下載</a>
			</div>';
			break;
		default:
	}
}
function echoModPage($connection, $modNewsNo)
{
	$result = mysqli_query($connection, "SELECT * FROM CSE_News_RR WHERE No = '" . $modNewsNo . "'");
	if (mysqli_num_rows($result) == 0) {
		header("Location: index.php?msg=nodata");
		exit();
	} else {
		$row = mysqli_fetch_assoc($result);
		echo '<h1>修改公告</h1>
				<div class="addNewsForm">
					<form action="../modnews.php" method="post" enctype="multipart/form-data">
						<div class="controls">
							<label for="Title">標題</label>
							<input name="Title" type="text" placeholder="請輸入標題" value="' . $row['Title'] . '" required/>
						</div>
						<div class="controls">
							<label for="Date">日期</label>
							<input name="Date" type="date" value="' . $row['Date'] . '" required/>
						</div>
						<div class="controls">
							<label for="Study">研習活動</label>
							<input id="Study" name="Study" ';
		if ($row['Type'] == 1) {
			echo 'checked';
		}
		echo ' type="checkbox"/>
						</div>
						<div class="controls">
							<input name="Upload[]" type="file" multiple/>
						</div>
						<div class="controls">
							<input id="link" type="text" placeholder="輸入欲插入的超連結網址"/><input type="button" onClick="insertLink()" value="插入超連結">
						</div>
						<script>
							function insertLink(){
								var textbox = document.getElementById("textbox");
								var link = document.getElementById("link");
								var l = link.value;
								textbox.value = textbox.value.substring(0,textbox.selectionEnd) + "<a href=\""+l+"\">" + l + "</a>" + textbox.value.substring(textbox.selectionEnd,textbox.value.length);
								link.value = "";
								textbox.focus();
							}
						</script>
						<div class="controls">
							<textarea id="textbox" name="Content" placeholder="請輸入公告內文" wrap="virtual" draggable="false">' . $row['Content'] . '</textarea>
						</div>
						<input type="hidden" name="ModNewsNo" value="' . $modNewsNo . '"/>
						<div class="controls">
							<input type="submit" value="送出"></input>
							<a href="index.php">
								<span class="Cancel">取消</span>
							</a>
						</div>
					</form>
				</div>';
	}
}
function echoModMember($memberNo, $connection)
{
	$result = mysqli_query($connection, 'SELECT * FROM CSE_Members_RR WHERE No=\'' . $memberNo . '\'');
	if (mysqli_num_rows($result) == 1) {
		$row = mysqli_fetch_assoc($result);
		echo '<h1>修改成員資料-' . $row['Name'] . '</h1>
			<div class="addNewsForm">
				<form action="../modmember.php" method="post" enctype="multipart/form-data">
					<div class="controls">
							<label for="Name">姓名</label>
							<input name="Name" type="text" placeholder="請輸入姓名" value="' . $row['Name'] . '" required/>
					</div>
					<div class="controls">
						<label for="Cate">職位</label>
						<select name="Cate">';
		$selectList = array("", "", "", "", "", "", "", "", "", "", "", "");
		$selectList[intval($row['Cate'])] = "selected";
		echo '<option value="0" ' . $selectList[0] . '>中心主任</option>
							<option value="1" ' . $selectList[1] . '>系主任</option>
							<option value="2" ' . $selectList[2] . '>各類輔導教授-視覺障礙類</option>
							<option value="4" ' . $selectList[4] . '>各類輔導教授-聽覺障礙類</option>
							<option value="3" ' . $selectList[3] . '>各類輔導教授-肢體障礙類</option>
							<option value="5" ' . $selectList[5] . '>各類輔導教授-情緒障礙類</option>
							<option value="11" ' . $selectList[11] . '>各類輔導教授-學習障礙類</option>
							<option value="6" ' . $selectList[6] . '>各類輔導教授-身體病弱類</option>
							<option value="7" ' . $selectList[7] . '>資源教室輔導員</option>
							<option value="8" ' . $selectList[8] . '>人力資源-特殊教育中心</option>
							<!--
							<option value="9" ' . $selectList[9] . '>人力資源-特殊教育學系</option>
							<option value="10" ' . $selectList[10] . '>人力資源-視障教育與重建中心</option>
							-->
						</select>
					</div>
					<div class="controls">
						<label for="Info">成員資訊</label>
						<textarea name="Info" placeholder="請輸入成員資訊" wrap="virtual" draggable="false" required>' . $row['Info'] . '</textarea>
					</div>
					<div class="controls">
							<label for="Email">Email</label>
							<input name="Email" type="text" placeholder="請輸入Email" value="' . $row['Email'] . '" required/>
					</div>
					<div class="controls">
						<label for="Upload">個人照片</label>
						<br/>
						<input name="Upload" type="file"/>
					</div>
					<input name="ModMemberNo" type="hidden" value="' . $memberNo . '"/>
					<div class="controls">
						<input type="submit" value="送出"></input>
						<a href="index.php">
								<span class="Cancel">取消</span>
						</a>
					</div>
				</form>
			</div>';
	} else {
		header("Location: index.php?msg=nodata");
	}
}
function echoModFix($modfixNo, $connection)
{
	$result = mysqli_query($connection, 'SELECT * FROM CSE_Fix_Contents_RR WHERE No=\'' . $modfixNo . '\'');
	$uploadFixNo = array(17, 19, 21);
	if (mysqli_num_rows($result) == 1) {
		$row = mysqli_fetch_assoc($result);
		if (in_array($modfixNo, $uploadFixNo)) {
			echo '<h1>修改頁面-' . $row['Name'] . '</h1>
				<div class="addNewsForm">
					<form action="../modfix.php" method="post" enctype="multipart/form-data">
						<h3>請上傳' . $row['Name'] . '的圖片</h3>
						<div class="controls">
							<input name="Image" type="file"/>
						</div>
						<input name="ModFixNo" type="hidden" value="' . $modfixNo . '"/>
						<div class="controls">
							<input type="submit" value="送出"></input>
							<a href="index.php">
									<span class="Cancel">取消</span>
							</a>
						</div>
					</form>
				</div>';
		} else {
			echo '<h1>修改頁面-' . $row['Name'] . '</h1>
				<div class="addNewsForm">
					<form action="../modfix.php" method="post">
						<div class="controls">
							<textarea name="Content" placeholder="請輸入欲修改成甚麼內容" wrap="virtual" draggable="false">' . $row['Content'] . '</textarea>
						</div>
						<input name="ModFixNo" type="hidden" value="' . $modfixNo . '"/>
						<div class="controls">
							<input type="submit" value="送出"></input>
							<a href="index.php">
									<span class="Cancel">取消</span>
							</a>
						</div>
					</form>
				</div>';
		}
	} else {
		header("Location: index.php?msg=nodata");
	}
}
function echoModForm($modformNo, $connection)
{
	$result = mysqli_query($connection, 'SELECT * FROM CSE_Forms_RR WHERE No=\'' . $modformNo . '\'');
	if (mysqli_num_rows($result) == 1) {
		$row = mysqli_fetch_assoc($result);
		echo '<h1>修改頁面-' . $row['Name'] . '</h1>
			<div class="addNewsForm">
				<form action="../modform.php" method="post" enctype="multipart/form-data">
					<div class="controls">
						<label for="Name">檔案名稱</label>
						<input name="Name" type="text" value="' . $row['Name'] . '" required/>
					</div>
					<h3>請上傳' . $row['Name'] . '的檔案</h3>
					<div class="controls">
						<input name="File" type="file"/>
					</div>
					<input name="ModFormNo" type="hidden" value="' . $modformNo . '"/>
					<div class="controls">
						<input type="submit" value="送出"></input>
						<a href="index.php">
								<span class="Cancel">取消</span>
						</a>
					</div>
				</form>
			</div>';
	} else {
		header("Location: index.php?msg=nodata");
	}
}
function echoModLaw($modlawNo, $connection)
{
	$result = mysqli_query($connection, 'SELECT * FROM CSE_Laws_RR WHERE No=\'' . $modlawNo . '\'');
	if (mysqli_num_rows($result) == 1) {
		$row = mysqli_fetch_assoc($result);
		echo '<h1>修改頁面-' . $row['Name'] . '</h1>
			<div class="addNewsForm">
				<form action="../modlaw.php" method="post" enctype="multipart/form-data">
					<div class="controls">
						<label for="Name">檔案名稱</label>
						<input name="Name" type="text" value="' . $row['Name'] . '" required/>
					</div>
					<h3>請上傳' . $row['Name'] . '的檔案</h3>
					<div class="controls">
						<input name="File" type="file"/>
					</div>
					<input name="ModLawNo" type="hidden" value="' . $modlawNo . '"/>
					<div class="controls">
						<input type="submit" value="送出"></input>
						<a href="index.php">
								<span class="Cancel">取消</span>
						</a>
					</div>
				</form>
			</div>';
	} else {
		header("Location: index.php?msg=nodata");
	}
}
function echoModLink($modlinkNo, $connection)
{
	$result = mysqli_query($connection, 'SELECT * FROM CSE_Links_RR WHERE No=\'' . $modlinkNo . '\'');
	if (mysqli_num_rows($result) == 1) {
		$row = mysqli_fetch_assoc($result);
		echo '<h1>修改頁面-' . $row['Name'] . '</h1>
			<div class="addNewsForm">
				<form action="../modlink.php" method="post">
					<div class="controls">
						<label for="Name">連結名稱</label>
						<input name="Name" type="text" value="' . $row['Name'] . '" required/>
					</div>
					<div class="controls">
						<label for="Cate">連結分類</label>
						<select name="Cate">';
		$selectList = array("", "", "", "");
		$selectList[intval($row['Cate'])] = "selected";
		echo '<option value="1" ' . $selectList[1] . '>校內行政資源</option>
							<option value="2" ' . $selectList[2] . '>各區特教中心</option>
							<option value="3" ' . $selectList[3] . '>校外好站連結</option>
						</select>
					</div>
					<div class="controls">
						<label for="HyperLink">連結網址</label>
						<input name="HyperLink" type="text" value="' . $row['HyperLink'] . '" required/>
					</div>
					<input name="ModLinkNo" type="hidden" value="' . $modlinkNo . '"/>
					<div class="controls">
						<input type="submit" value="送出"></input>
						<a href="index.php">
								<span class="Cancel">取消</span>
						</a>
					</div>
				</form>
			</div>';
	} else {
		header("Location: index.php?msg=nodata");
	}
}
function echoNews($connection, $NewsNo)
{
	$result = mysqli_query($connection, "SELECT * FROM CSE_News_RR WHERE No='" . $NewsNo . "'");
	if (mysqli_num_rows($result) == 0) {
		echo '<div class="mainBlockContent">
				<div class="informationBlock">
				<h1 class="newsPageTitle">查無此公告</h1>
				</div>
				</div>';
	} else {
		$rows = mysqli_fetch_assoc($result);
		echo '<div class="mainBlockContent">
				<div class="informationBlock">
				<h1 class="newsPageTitle">' . $rows['Title'] . '</h1>';
		if ($rows['Type'] == '1') {
			echo '<h3 class="newsPageType">研習活動</h3>';
		}
		if (isset($_SESSION['account'])) {
			echo '<h4><a class="delNewsA" href="?msg=delnews&delnewsno=' . $NewsNo . '">刪除此貼文</a><h4>
				<h4><a class="delNewsA" href="?modnewsno=' . $NewsNo . '">修改此貼文</a><h4>';
		}
		echo '<label class="newsPageDate">日期:' . $rows['Date'] . '</label>
			</div>';
		if ($rows['File'] != "") {
			echo '<div class="newsPageImg">';
			foreach (explode(';', $rows['File']) as $filename) {
				if (file_exists("../uploads/" . $filename) and substr(mime_content_type("../uploads/" . $filename), 0, 5) == "image") {
					echo '
							<img src="../uploads/' . $filename . '" alt="' . $rows['Title'] . '(附圖)-' . $filename . '"/>
						';
				}
			}
			echo '</div>';
			foreach (explode(';', $rows['File']) as $filename) {
				if ($filename != "") {
					echo '<div class="fileBlock">
							<a href="../uploads/' . $filename . '" download>
							<span class="downloadBtn" alt="' . $filename . '">下載檔案-' . $filename . '</span>
							</a>
							</div>';
				}
			}
		}
		echo '<p class="newsPageContent">' . nl2br($rows['Content']) . '</p>
				</div>';
	}
}
