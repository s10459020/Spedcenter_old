<!doctype html>
<html lang="zh-Hant-TW">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0,height=device-height,user-scalable=0">
<link rel="stylesheet" type="text/css" href="CSS/index.css"/>
<title>無標題文件</title>
<?php
	$DBname = "s10459002";
    require_once("../Functions/SQLFunctions.php");
	session_start();
	$conncetion = conncetSQLDB($DBname);
?>
</head>

<body>
	<div class="TopBar">
    	<span class="TitleImage">
        	<img src="Image/maintitle_shadow.png" id="maintitle" alt="國立臺南大學特殊教育中心 標題文字圖片"/>
        	<img src="Image/newTitle.jpg" id="maintitleback" alt="國立臺南大學特殊教育中心 標題文字背景圖片"/>
        </span>
        <div class="HorNavigation">
        	<a href="?page=siteMap"><span class="HorItem">網站導覽</span></a>
        	<a href="?page=index"><span class="HorItem">繁體中文</span></a>
        	<a href="?page=siteMap"><span class="HorItem">English</span></a>
        	<a href="siteMap.html"><span class="HorItem">資源教室</span></a>
        	<a href="siteMap.html"><span class="HorItem">台南大學首頁</span></a>
        </div>
    </div>
    <div class="mainBlock">
    	<span class="leftBlock">
        	<span class="NavigationBar">
        	<span class="colorblock">站內連結</span>
            <a href="..">
                <span class="NavSelection">
            	<img src="Image/linkIconHomepage.png" alt="首頁圖標"/>首頁</span>
            </a>
        	<span class="colorblock smalllinkblock"></span>
            <a href="?page=siteMap">
                <span class="NavSelection">
            	<img src="Image/linkIconMap.png" alt="網站導覽圖標"/>網站導覽</span>
            </a>
        	<span class="colorblock smalllinkblock"></span>
            <a href="?page=index">
                <span class="NavSelection">
            	<img src="Image/linkIconNews.png" alt="最新消息圖標"/>最新消息</span>
            </a>
        	<span class="colorblock smalllinkblock"></span>
            <span class="NavSelection" id="AboutCSE">
                <a href="?page=AboutCSE" class="innerSpan">
            	<img src="Image/linkIconAbout.png" alt="關於特教中心圖標"/>關於特教中心</a>
                <div class="Dropdown">
                    <a href="?page=introduction">
                        <span class="DropDownItem">中心簡介</span>
                    </a>
                    <a href="?page=outline">
                        <span class="DropDownItem">業務概況</span>
                    </a>
                    <a href="?page=consultation">
                        <span class="DropDownItem">諮詢專線</span>
                    </a>
<!--                    <a href="http://ask.set.edu.tw/default.asp?schid=210036" target="_blank">
                        <span class="DropDownItem">Q&amp;A</span>
                    </a>
-->                    <a href="?page=collection">
                        <span class="DropDownItem">叢書</span>
                    </a>
                    <a href="?page=testRegulation">
                        <span class="DropDownItem">測驗申請</span>
                    </a>
                </div>
            </span>
        	<span class="colorblock smalllinkblock"></span>
            <span class="NavSelection" id="ArticleNActivity">
                <a href="?page=ArticleNActivity" class="innerSpan">
            	<img src="Image/linkIconActivity.png" alt="活動與資訊圖標"/>活動與資訊</a>
                <div class="Dropdown">
<!--                    <a href="messages.html">
                        <span class="DropDownItem">特教簡訊</span>
                    </a>
-->                    <a href="?page=index&activities=1">
                        <span class="DropDownItem">研習活動</span>
                    </a>
<!--                    <a href="downloads.html">
                        <span class="DropDownItem">資料下載</span>
                    </a>
--><!--                    <a href="materials.html">
                        <span class="DropDownItem">教材教具</span>
                    </a>
--><!--                    <a href="references.html">
                        <span class="DropDownItem">參考書目</span>
                    </a>
-->                </div>
            </span>
        	<span class="colorblock smalllinkblock"></span>
            <span class="NavSelection" id="ContactInfo">
                <a href="?page=ContactInfo" class="innerSpan">
            	<img src="Image/linkIconContact.png" alt="意見反映圖標"/>意見反應</a>
                <div class="Dropdown">
                    <a href="mailto:gloria@mail.nutn.edu.tw">
                        <span class="DropDownItem">意見信箱</span>
                    </a>
                    <a href="http://campus.nutn.edu.tw/netForum/default.aspx" target="_blank">
                        <span class="DropDownItem">留言板</span>
                    </a>
                </div>
            </span>
        	<span class="colorblock smalllinkblock"></span>
        </span>
        	<span class="NavigationBar">
        		<span class="colorblock">聯絡我們</span>
                <a href="mailto:gloria@mail.nutn.edu.tw">
                    <span class="NavSelection">
                    <img src="Image/linkIconMap.png" alt="網站導覽圖標"/>電子信箱</span>
                </a>
                <a href="http://campus.nutn.edu.tw/netForum/default.aspx">
                    <span class="NavSelection">
                    <img src="Image/linkIconMap.png" alt="網站導覽圖標"/>電子留言板</span>
                </a>
            </span>
        </span>
        <span class="colorblock distinct"></span>
        <div class="CenterBlock">
        <?php
			if(isset($_GET['page']) and $_GET['page']=='outline'){
				echo '<span class="colorblock"></span>
				<div class="SinglePageBlock">
					<div class="blockTitle">
						<h2>
							<img src="Image/linkIconAbout.png" alt="業務概況 圖標"/>業務概況
							<span class="colorblock smalllinkblock"></span>
						</h2>
					</div>
					<div class="outlineblock">
						<h3>服務項目</h3>
						<ol type="1">
							<li>特殊學生之鑑定、就學及心理輔導。</li>
							<li>特殊教育法規及疑難問題之諮詢。 </li>
							<li>特殊教育教學問題之研究及諮詢。 </li>
							<li>特殊教育教學資源之交換與提供。 </li>
							<li>特殊學生家長親職教育之問題。 </li>
							<li>特殊個案之輔導諮詢及追蹤研究。 </li>
							<li>特殊教育教學實務問題研討。 </li>
							<li>其他有關特教教育事項之辦理。</li>
						</ol>
						<h3>服務對象</h3>
						<ol>
							<li>中小學特殊教育教師、行政人員、教保人員、特殊學生及家長。</li>
						</ol>
						<h3>諮詢人員</h3>
						<ol type="1">
							<li>莊妙芬　重度障礙、智能障礙</li>
							<li>吳昆壽　資賦優異、殘障資優</li>
							<li>邢敏華　聽覺障礙、手語教學</li>
							<li>楊憲明　學習障礙</li>
							<li>李芃娟　聽覺障礙</li>
							<li>林慶仁　視覺障礙、職業復健</li>
							<li>林淑玟　復健諮商、肢體障礙、輔助科技</li>
							<li>詹士宜　學習障礙</li>
							<li>曾怡惇　視覺障礙、語言障礙</li>
							<li>朱慧娟　情緒障礙、多重障礙</li>
							<li>何美慧　自閉症、情緒障礙</li>
							<li>高振耀　資賦優異</li>
							<li>曾明基　肢體障礙、輔助科技</li>
							<li>林千玉　互動科技、輔助科技</li>
							<li>陳家成　健康諮詢、醫療照護</li>
							<li>林健禾　妥瑞、自閉症及過動症診斷</li>
						</ol>
					</div>
				</div>';
			}else
			if(isset($_GET['page']) and $_GET['page']=='consultation'){
				echo '<span class="colorblock"></span>
				<div class="SinglePageBlock">
					<div class="blockTitle">
						<h2>
							<img src="Image/linkIconAbout.png" alt="諮詢專線 圖標"/>諮詢專線
							<span class="colorblock smalllinkblock"></span>
						</h2>
					</div>
					<div class="counsulationblock">
						<img src="Image/諮詢專線-橫幅1.jpg"/>
						<img src="Image/諮詢專線-裁切1.jpg"/>
						<img src="Image/諮詢專線-裁切2.jpg"/>
					</div>
				</div>';
			}else
			if(isset($_GET['page']) and $_GET['page']=='collection'){
				echo '<span class="colorblock"></span>
            <div class="SinglePageBlock">
                    <div class="blockTitle">
                        <h2>
                            <img src="Image/linkIconAbout.png" alt="叢書 圖標"/>叢書
                            <span class="colorblock smalllinkblock"></span>
                        </h2>
                    </div>
					<table class="colletiontable" cols="4">
						 <tr>
            <th bgcolor="#EFECE2">編號</th>
            <th bgcolor="#EFECE2">作者</th>
            <th bgcolor="#EFECE2">書名</th>
            <th bgcolor="#EFECE2">備註</th>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">1</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">黃奇汪　著</td>
            <td bgcolor="#EFECE2">生活經驗統整課程之設計</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">*</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">2</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">林寶貴　編著</td>
            <td bgcolor="#EFECE2">特殊兒童課程編製與教學設計之理論與實際</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">*</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">3</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">林寶貴　編著</td>
            <td bgcolor="#EFECE2">語言發展與矯治</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">*</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">4</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">林寶貴　編著</td>
            <td bgcolor="#EFECE2">特殊兒童語文訓練與知動訓練教材教法</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">*</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">5</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">陳英三　編著</td>
            <td bgcolor="#EFECE2">障礙兒童的動作教育</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">*</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">6</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">黃奇汪等　編</td>
            <td bgcolor="#EFECE2">國民小學啟智班生活經驗統整課程實驗教學教師手冊（一）</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">*</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">7</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">吳知賢　譯</td>
            <td bgcolor="#EFECE2">輕度智能不足兒童之推理能力</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">8</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">蔡秋桃　譯</td>
            <td bgcolor="#EFECE2">如何對特殊兒童的父母進行諮商</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">*</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">9</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">劉信雄、蔡秋桃　譯</td>
            <td bgcolor="#EFECE2">麥克司佛與布希弘茲學前盲童社會成熟量表實施手冊</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">10</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">劉信雄、吳知贀　編著</td>
            <td bgcolor="#EFECE2">視覺障礙學生的教學與輔導</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">11</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">何華國　著</td>
            <td bgcolor="#EFECE2">啟智班個別化教學設計</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">*</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">12</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">何華國、吳昆壽　主編</td>
            <td bgcolor="#EFECE2">特殊教育輔導工作手冊</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">*</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">13</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">陳英豪、何華國、李芃娟　主編</td>
            <td bgcolor="#EFECE2">中重度啟智班生活教育教學活動設計</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">*</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">14</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">陳小娟、林淑玟　主編</td>
            <td bgcolor="#EFECE2">語調聽覺法</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">15</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">陳英豪、何華國、吳昆壽　主編</td>
            <td bgcolor="#EFECE2">啟智學校（班）美勞科教學活動設計</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">16</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">蔡秋桃　譯著</td>
            <td bgcolor="#EFECE2">幼兒發展之進度－出生至五歲</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">*</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">17</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">吳昆壽　著</td>
            <td bgcolor="#EFECE2">國小資優生與普通生認知風格、學習策略與學業成就關係之研究</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">18</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">陳小娟　著</td>
            <td bgcolor="#EFECE2">啟聰學校國小溝通訓練教材</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">19</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">陳小娟　著</td>
            <td bgcolor="#EFECE2">啟聰學校國中溝通訓練教材</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">20</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">陳小娟　編著</td>
            <td bgcolor="#EFECE2">聽障教育彙編</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">21</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">王亦榮　撰</td>
            <td bgcolor="#EFECE2">大專院校視覺障礙學生輔導工作手冊</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">22</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">何華國　等編</td>
            <td bgcolor="#EFECE2">啟智班音樂教材彙編</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">23</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">邢敏華　著</td>
            <td bgcolor="#EFECE2">特殊教育專題選輯</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">24</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">莊妙芬　著</td>
            <td bgcolor="#EFECE2">重度障礙之理論與實務（一）</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">25</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">莊妙芬　等著</td>
            <td bgcolor="#EFECE2">在家自行教育學生輔助需求調查</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">*</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">26</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">莊妙芬　等著</td>
            <td bgcolor="#EFECE2">學前特殊幼兒教育需求評估</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">*</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">27</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">陳英三　編著</td>
            <td bgcolor="#EFECE2">特殊教育教材教法－感覺運動指導篇</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">28</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">莊妙芬、吳昆壽　等著</td>
            <td bgcolor="#EFECE2">身心障礙教師專業知能調查</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">29</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">楊憲明　著</td>
            <td bgcolor="#EFECE2">學習障礙</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">30</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">黃秀霜　著</td>
            <td bgcolor="#EFECE2">閱讀障礙理論與實務</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">31</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">李芃娟　編著</td>
            <td bgcolor="#EFECE2">聽覺障礙兒童教學方法</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">32</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">李芃娟　著</td>
            <td bgcolor="#EFECE2">塞擦音聽的知覺清晰度與聲學特質</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">33</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">吳昆壽　著</td>
            <td bgcolor="#EFECE2">資賦優異兒童教育</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">34</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">吳昆壽　編</td>
            <td bgcolor="#EFECE2">資優教育－課程與教學</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">35</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">吳昆壽　編</td>
            <td bgcolor="#EFECE2">資優教育－教學實務篇</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">36</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">吳昆壽　著</td>
            <td bgcolor="#EFECE2">資優殘障學生教育現況與相關問題探析</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">*</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">37</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">楊憲明　著</td>
            <td bgcolor="#EFECE2">閱讀障礙視知覺機制缺陷及中文文字表徵獨特影響之探究</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">*</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">38</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">趙佳文　著</td>
            <td bgcolor="#EFECE2">國小弱視學生歸因方式、成就動機與學業成就關係之研究</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">39</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">邢敏華　著</td>
            <td bgcolor="#EFECE2">有效教育聽覺障礙學生</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">40</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">鄒啟蓉　著</td>
            <td bgcolor="#EFECE2">發展遲緩兒在融合教育環境中社會行為表現研究</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">41</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">鄭靜宜　著</td>
            <td bgcolor="#EFECE2"><a href="../book/Book41.pdf" target="_blank">增進學生的口語溝通能力</a></td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">42</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">李芃娟　著</td>
            <td bgcolor="#EFECE2">聽障兒童之聽能說話訓練</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">43</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">林慶仁　著</td>
            <td bgcolor="#EFECE2">弱視教育與重建</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">44</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">林淑玟　著</td>
            <td bgcolor="#EFECE2">無障礙理念的省思與應用（上）</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">45</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">林淑玟　著</td>
            <td bgcolor="#EFECE2">無障礙理念的省思與應用（下）</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">46</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">鄭美芝、高瑞鄉、何嘉雯、翁芸生、林和秀</td>
            <td bgcolor="#EFECE2"><a href="http://www2.nutn.edu.tw/gac646/book/46.zip" target="_blank">資源班基本識字教材使用手冊</a></td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">47</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
            <td bgcolor="#EFECE2"><a href="http://www2.nutn.edu.tw/gac646/book/book47.htm" target="_blank">2004年資優教育學術研討會論文集</a></td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">48</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">何美慧　著</td>
            <td bgcolor="#EFECE2">向裏看、向外看 　─體會自閉症─</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">49</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">何美慧　彙編</td>
            <td bgcolor="#EFECE2"><a href="http://www2.nutn.edu.tw/gac646/book/%E7%89%B9%E6%95%99%E5%8F%A2%E6%9B%B8%E7%AC%AC49%E8%BC%AF-EBD%E5%B7%A5%E4%BD%9C%E5%9D%8A%E5%A0%B1%E5%91%8A%E5%BD%99%E7%B7%A8.rar" target="_blank">EBD工作坊報告彙編</a></td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">50</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">曾怡惇　著</td>
            <td bgcolor="#EFECE2"><a href="http://www2.nutn.edu.tw/gac646/book/50%E8%BC%AF%E5%85%A8%E6%9C%AC.pdf" target="_blank">國小初任特教教師採用「情境教學策略」教導智能障礙學生溝通效果之研究</a></td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
		            <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">51</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">李芃娟　著</td>
            <td bgcolor="#EFECE2">聽障學童聽能說話訓練電腦輔助教學系統</td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
		            <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">52</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">林淑玟　著</td>
            <td bgcolor="#EFECE2"><a href="../book/Book52.pdf" target="_blank">跨專業輔助科技整合服務團隊之運作</a></td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
		  		            <tr>
            <td align="center" valign="middle" bgcolor="#EFECE2">53</td>
            <td align="left" valign="middle" bgcolor="#EFECE2">陳英三　著</td>
            <td bgcolor="#EFECE2"><a href="../book/53-&#30693;&#35258;&#21205;&#20316;&#35347;&#32244;&#9472;&#29702;&#35542;&#33287;&#23526;&#38555;%20&#31995;&#21015;&#19968;.pdf" target="_blank">知覺動作訓練理論與實際系列一</a></td>
            <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
          </tr>
		  		  		            <tr>
		  		              <td align="center" valign="middle" bgcolor="#EFECE2">54</td>
		  		              <td align="left" valign="middle" bgcolor="#EFECE2">陳英三　著</td>
                              <td bgcolor="#EFECE2"><a href="../book/&#30693;&#35258;&#21205;&#20316;&#35347;&#32244;&#9472;&#9472;&#29702;&#35542;&#33287;&#23526;&#38555;(&#31532;&#20108;&#20874;).pdf" target="_blank">知覺動作訓練理論與實際系列二</a></td>
                              <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
              </tr>
			   <tr>
		  		              <td align="center" valign="middle" bgcolor="#EFECE2">55</td>
		  		              <td align="left" valign="middle" bgcolor="#EFECE2">陳英三　著</td>
                              <td bgcolor="#EFECE2"><a href="../book/&#21474;&#26360;55.pdf" target="_blank">知覺動作訓練理論與實際系列三、四</a></td>
                              <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
              </tr>
			   <tr>
		  		              <td align="center" valign="middle" bgcolor="#EFECE2">56</td>
		  		              <td align="left" valign="middle" bgcolor="#EFECE2">李芃娟　著</td>
                              <td bgcolor="#EFECE2"><a href="../book/&#29305;&#25945;&#21474;&#26360;&#31532;56&#36655;.pdf" target="_blank">聽覺障礙</a></td>
                              <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
              </tr>
		  		            <tr>
		  		              <td align="center" valign="middle" bgcolor="#EFECE2">57</td>
		  		              <td align="left" valign="middle" bgcolor="#EFECE2">詹士宜　主編</td>
                              <td bgcolor="#EFECE2"><a href="../book/&#29305;&#25945;&#21474;&#26360;&#31532;57&#36655;.pdf" target="_blank">替換式數學對數學學習困難學生之補教教學</a></td>
                              <td align="center" valign="middle" bgcolor="#EFECE2">&nbsp;</td>
              </tr>
		  <tr align="left">
            <td colspan="4" valign="middle" bgcolor="#EFECE2"><span class="style1">備註*字號者，為特教中心無庫存之意。</span></td>
            </tr>

					</table>
			</div>';
			}else
			if(isset($_GET['page']) and $_GET['page']=='testRegulation'){
				echo '
        	<span class="colorblock"></span>
            <div class="SinglePageBlock">
            	<div class="blockTitle">
                	<h2>
                        <img src="Image/linkIconMap.png" alt="測驗申請 圖標"/>測驗申請
                        <span class="colorblock smalllinkblock"></span>
                    </h2>
                </div>
				<div class="testRegulationBlock">
					<h3>特教中心測驗工具借閱需知</h3>
					<ol>
					  <li> 校內單位僅限特教系師生借用，本系學生借閱測驗者，申請表需指導教授<span style="color:rgb(200,50,50);">簽名</span>。</li>
					  <li>若借閱之測驗工具為教師授課之用，請依借用規則，事先向特教中心辦理借用手續。 </li>
					  <li>特殊教育需求學生診斷用測驗工具，得由專業測驗人員以診斷使用為由方可借用。</li>
					  <li>校外單位（<span style="color:rgb(200,50,50);">限本校輔導區</span>）需依據<span style="color:rgb(200,50,50);">公文</span>，並填寫<span style="color:rgb(200,50,50);"">測驗申請書</span>始能借閱測驗工具。如欲借「魏氏智力測驗」，除公文外，尚需押<span style="color:rgb(200,50,50);">魏氏智力測驗研習證書(一張證書限借一份)</span>。</li>
					  <li>借閱期限為<span style="color:rgb(200,50,50);">2週</span>，逾期則需自行至特教中心辦理續借；續借以1次為限，續借期限以2週為限。</li>
					  <li>測驗出借前，借用人請先行檢查測驗內容，歸還時若有毀損、污損、遺失等，應由借用人負責，按測驗原價格<span style="color:rgb(200,50,50);">加倍賠償</span>，並<span style="color:rgb(200,50,50);"><strong>停止借用測驗六個月</strong></span>。</li>
					  <li>測驗工具之消耗品，如測驗紙、紀錄本、篩選紙等，本中心只提供借閱使用，不作為消費用途，請勿在消耗品上做記號。</li>
					  <li>歸還測驗時需由特教中心相關人員點收並簽名。</li>
					  <li>使用者對於評量工具之內容請<span style="color:rgb(200,50,50);"><strong>遵守測驗使用倫理</strong></span>應嚴加保密，絕對不可複製。</li>
					  <li>請填妥測驗借用申請表內相關資料。<br />
					</ol>
					<a href="../uploads/測驗申請表.doc">測驗申請表下載</a>
				</div>
			</div>';
				
			}else
			if(isset($_GET['page']) and $_GET['page']=='siteMap'){
				echo '
        	<span class="colorblock"></span>
            <div class="SinglePageBlock">
            	<div class="blockTitle">
                	<h2>
                        <img src="Image/linkIconMap.png" alt="網站導覽 圖標"/>網站導覽
                        <span class="colorblock smalllinkblock"></span>
                    </h2>
                </div>
                <div class="MapBlock">
                    <a href="index.php">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            最新消息
                        </span>
                    </a>
                    <a href="?page=introduction">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            中心簡介</span>
                    </a>
                    <a href="?page=outline">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            業務概況</span>
                    </a>
                    <a href="?page=consultation">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            諮詢專線</span>
                    </a>
<!--                    <a href="http://ask.set.edu.tw/default.asp?schid=210036" target="_blank">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            Q&amp;A</span>
                    </a>
-->                    <a href="?page=collection">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            叢書</span>
                    </a>
                    <a href="?page=testRegulation">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            測驗申請</span>
                    </a>
<!--                    <a href="?page=messages">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            特教簡訊</span>
                    </a>
-->                    <a href="?page=activities">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            研習活動</span>
                    </a>
                    <a href="?page=downloads">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            資料下載</span>
                    </a>
                    <a href="?page=materials">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            教材教具</span>
                    </a>
                    <a href="?page=references">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            參考書目</span>
                    </a>
                    <a href="mailto:gloria@mail.nutn.edu.tw">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            意見信箱</span>
                    </a>
                    <a href="http://campus.nutn.edu.tw/netForum/default.aspx" target="_blank">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            留言板</span>
                    </a>
                </div>
            </div>';
			}else
			if(isset($_GET['page']) and $_GET['page']=='introduction'){
				echo '
            <span class="colorblock"></span>
            <div class="SinglePageBlock">
                    <div class="blockTitle">
                        <h2>
                            <img src="Image/linkIconAbout.png" alt="關於特教中心 圖標"/>關於特教中心
                            <span class="colorblock smalllinkblock"></span>
                        </h2>
                    </div>
                    <div class="introductionContent">
                        <h3>沿革</h3>
                        <p>特殊教育中心於民國69年成立，設主任一人。民國76年成立特殊教育系後， 中心主任由特教系主任兼任之。民國96年起茲因輔導區服務範圍擴及學前、國小、國中、高中職教育階段，且特教中心同時經營本校資源教室，中心主任由特教系專任副教授以上教師兼任之。</p>
                        <h3>組織</h3>
                        <ol>
                            <li>本中心置主任一人，由校長遴聘副教授以上專任教師兼任，綜理本中心業務。</li>
                            <li>本中心設研究與輔導兩組。研究組員負責辦理有關特殊教育研究、出版、資料 蒐集、交換等事宜；輔導組辦理有關特殊教育研究計劃、特殊學生鑑定、 教學輔導、諮詢服務、訓練及特殊教育推廣等事宜。</li>
                            <li>本中心設置特殊教育諮詢專線，提供南區特殊教育諮詢與服務。</li>
                            <li>本中心聘專案助理一人，辦理中心各項行政業務。</li>
                            <li>本中心設置資源教室，輔導及服務本校特殊需求學生。</li>
                        </ol>
                        <h3>設備</h3>
                        <ol>
                            <li>中心辦公室1間，兼諮詢專線。</li>
                            <li>測驗教具陳列室1間。</li>
                            <li>特教叢書陳放室(與視障師資訓練班視障叢書共用地下室1間)。</li>
                            <li>資源教室1間(與特教系圖書室共用)。</li>
                        </ol>
                        <h3>人員</h3>
                        <table>
                            <tr>
                                <td>
                                    <img src="../Img/img020.jpg" alt="楊憲明 副教授" />
                                </td>
                                <td>
                                    <ul>
                                        <li>主任：楊憲明 副教授</li>
                                        <li>學歷：美國伊利諾大學博士</li>
                                        <li>專長領域：學習障礙</li>
                                        <li>連絡電話：06-2133111分機639、746</li>
                                        <li>電子信箱：<a href="mailto:hyang@mail.nutn.edu.tw">hyang@mail.nutn.edu.tw</a></li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="../Img/8.jpg" alt="羅秀雲 小姐" />
                                </td>
                                <td>
                                    <ul>
                                        <li>專案助理：羅秀雲 小姐</li>
                                        <li>工作職掌：教育部委辦之輔導區特殊教育相關業務；其它交辦事項。</li>
                                        <li>連絡電話：06-2133111分機645</li>
                                        <li>電子信箱：<a href="mailto:gloria@mail.nutn.edu.tw">gloria@mail.nutn.edu.tw</a></li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="../Img/is.jpg" alt="陳梅望 小姐" />
                                </td>
                                <td>
                                    <ul>
                                        <li>資源教室輔導員：陳梅望 小姐</li>
                                        <li>工作職掌：解決學生考試特殊需求；選課狀況調查；協助肢障輔具申請；處理學生個案生活、社會適應等；個案輔導會議；其它交辦事項。</li>
                                        <li>連絡電話：06-2133111分機644</li>
                                        <li>電子信箱<a href="mailto:：isabel87@mail.nutn.edu.tw">：isabel87@mail.nutn.edu.tw</a></li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="../Img/eva.jpg" alt="楊憲明 副教授" />
                                </td>
                                <td>
                                    <ul>
                                        <li>資源教室輔導員：張筱青 小姐</li>
                                        <li>工作職掌：經費核銷；辦理各類會議、活動及座談；教材製作；協助視障輔具申請；課業加強；轉銜服務； 網頁管理；其它交辦事項</li>
                                        <li>連絡電話：06-2133111分機646</li>
                                        <li>電子信箱：<a href="mailto:iamcici@mail.nutn.edu.tw">iamcici@mail.nutn.edu.tw</a></li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="../Img/Lin.jpg" alt="楊憲明 副教授" />
                                </td>
                                <td>
                                    <ul>
                                        <li>資源教室輔導員：林香綺 小姐</li>
                                        <li>工作職掌：辦理各類會議、活動及座談；轉銜服務；協助聽障輔具申請；教材製作； 其它交辦事項。</li>
                                        <li>連絡電話：06-2133111分機864</li>
                                        <li>電子信箱：<a href="mailto:chi1987@mail.nutn.edu.tw">chi1987@mail.nutn.edu.tw</a></li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </div>
           	 </div>';
			}else
        	if(isset($_GET['page']) and $_GET['page']=='ContactInfo'){
				echo '
        	<span class="colorblock"></span>
            <div class="SinglePageBlock">
            	<div class="blockTitle">
                	<h2>
                        <img src="Image/linkIconActivity.png" alt="活動與資訊 圖標"/>活動與資訊
                        <span class="colorblock smalllinkblock"></span>
                	</h2>
                </div>
                <div class="MapBlock">
                    <a href="mailto:gloria@mail.nutn.edu.tw">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            意見信箱</span>
                    </a>
                    <a href="http://campus.nutn.edu.tw/netForum/default.aspx" target="_blank">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            留言板</span>
                    </a>
                </div>
            </div>';
			}else
			if(isset($_GET['page']) and $_GET['page']=='ArticleNActivity'){
				echo '
        	<span class="colorblock"></span>
            <div class="SinglePageBlock">
            	<div class="blockTitle">
                	<h2>
                        <img src="Image/linkIconActivity.png" alt="活動與資訊 圖標"/>活動與資訊
                        <span class="colorblock smalllinkblock"></span>
                	</h2>
                </div>
        		     <div class="MapBlock">
<!--                 <a href="messages.html">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            特教簡訊</span>
                    </a>
-->                    <a href="?page=activities">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            研習活動</span>
                    </a>
<!--                    <a href="downloads.html">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            資料下載</span>
                    </a>
--><!--                    <a href="materials.html">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            教材教具</span>
                    </a>
--><!--                    <a href="references.html">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            參考書目</span>
                    </a>
-->                </div>
            </div>';
			}else
			if(isset($_GET['page']) and $_GET['page']=='AboutCSE'){
				echo '
        	<span class="colorblock"></span>
            <div class="SinglePageBlock">
            	<div class="blockTitle">
                	<h2>
                        <img src="Image/linkIconAbout.png" alt="關於特教中心 圖標"/>關於特教中心
                        <span class="colorblock smalllinkblock"></span>
                	</h2>
                </div>
                <div class="MapBlock">
                    <a href="?page=introduction">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            中心簡介</span>
                    </a>
                    <a href="?page=outline">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            業務概況</span>
                    </a>
                    <a href="?page=consultation">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            諮詢專線</span>
                    </a>
<!--                    <a href="http://ask.set.edu.tw/default.asp?schid=210036" target="_blank">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            Q&amp;A</span>
                    </a>
-->                    <a href="?page=collection">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            叢書</span>
                    </a>
                    <a href="?page=testRegulation">
                        <span class="MapItem">
                        	<span class="linksIcon"></span>
                            測驗申請</span>
                    </a>
                </div>
            </div>';
			}else{
				if(isset($_GET['activities'])){
					echo '
					<span class="colorblock">研習活動</span>
					<div class="MaskBlock">
						<div class="blockTitle">
							<h2>
								<img src="Image/linkIconNews.png" alt="最新消息圖標"/>研習活動
								<span class="colorblock smalllinkblock"></span>
							</h2>
						</div>
						<div class="NewsListBlock">';
				}else{
					echo '
					<span class="colorblock"></span>
					<div class="MaskBlock">
						<div class="blockTitle">
							<h2>
								<img src="Image/linkIconNews.png" alt="最新消息圖標"/>最新消息
								<span class="colorblock smalllinkblock"></span>
							</h2>
						</div>
						<div class="NewsListBlock">';
				}
                    if(isset($_SESSION['account'])){
                        echo '<a href="?msg=addnews" id="addNewsA">新增公告</a>';
                    }
					if(isset($_GET['activities'])){
              	      $result = mysqli_query($conncetion,"SELECT A1.Date,A1.type,A2.* FROM CSE_NewsList A1,CSE_NewsContent A2 WHERE A1.type = 'study' AND A2.NewsNo = A1.No ORDER BY A1.Date DESC , A2.NewsNo DESC");
					}else{
              	      $result = mysqli_query($conncetion,"SELECT A1.Date,A1.type,A2.* FROM CSE_NewsList A1,CSE_NewsContent A2 WHERE A2.NewsNo = A1.No ORDER BY A1.Date DESC , A2.NewsNo DESC");
					}
                    $preNo = -1;//儲存上一個公告內容的公告編號
					if(mysqli_num_rows($result)==0){
						echo '<span>查無資料。</span>';
					}
                    while($rows = mysqli_fetch_assoc($result)){
						$fileType = "";
						if($rows['type']=='study'){
							$rowstype = '[研習]';	
						}else{
							$rowstype = '';	
						}
                        if($rows['NewsNo'] != $preNo){
                            if($preNo != -1){
                                echo '</span>';
                            }
                            echo '<h4>'.$rows['Date'].'</h4>';
                            if(isset($_SESSION['account'])){
                                echo '<a href="?msg=delnews&delnewsno='.$rows['NewsNo'].'">[刪除]</a><br>';
                            }
                            echo '
                                    <span>
									<label>'.$rowstype.'</label>
                                    <a';
							if($rows['fileName']!=''){
                                echo ' href="../uploads/'.$rows['fileName'].'" ';
								$fileType = mime_content_type('../uploads/'.$rows['fileName']);
							}else if($rows['url']!=''){
                                echo ' href="'.$rows['url'].'" ';
                            }
                            echo '>'.$rows['Title'];
							if($fileType!="" and substr($fileType,0,5)=="image"){
								echo '<span class="imgPreview">
									<img src="../uploads/'.$rows['fileName'].'">
								</span>';
							}
							echo '</a>';
                        }else{
                            echo ',<a';
							if($rows['fileName']!=''){
                                echo ' href="../uploads/'.$rows['fileName'].'" ';
								$fileType = mime_content_type('../uploads/'.$rows['fileName']);
							}else if($rows['url']!=''){
                                echo ' href="'.$rows['url'].'" ';
                            }
                            echo '>'.$rows['Title'];
							if($fileType!="" and substr($fileType,0,5)=="image"){
								echo '<span class="imgPreview">
									<img src="../uploads/'.$rows['fileName'].'">
								</span>';
							}
							echo '</a>';
                        }
                        $preNo = $rows['NewsNo'];
                    }
                    echo "</span>
                </div>
            </div>";
			}
			?>
        </div>
        <span class="colorblock distinct"></span>
        <span class="LinkBlock">
        	<span class="colorblock">其他連結</span>
        	<span class="colorblock smalllinkblock"></span>
            <div class="links">
            	<span class="linksIcon"></span>
                <a class="linksA" href="http://www2.nutn.edu.tw/gac646/resource_new/index.asp" target="_blank" title="資源教室(開啟新視窗)">資源教室</a>
            </div>
            <div class="links">
            	<span class="linksIcon"></span>
                <a class="linksA" href="http://web.nutn.edu.tw" target="_blank" title="臺南大學首頁(開啟新視窗)">臺南大學首頁</a>
            </div>
            <div class="links">
            	<span class="linksIcon"></span>
                <a class="linksA" href="http://www2.nutn.edu.tw/gac646/resources.htm" target="_blank" title="全國特教網(開啟新視窗)">全國特教網</a>
            </div>
        </span>
        
        <span class="relativeLink LinkBlock">
        	<span class="colorblock">相關網站</span>
            <div class="links">
            	<span class="linksIcon"></span>
                <a class="linksA" href="http://www.edu.tw/" target="_blank" title="教育部網站(開啟新視窗)">教育部</a>
            </div>
            <div class="links">
            	<span class="linksIcon"></span>
                <a href="http://www.eduassistech.org/" target="_blank" title="教育部肢體障礙學習輔具中心">教育部肢體障礙學習輔具中心</a>
            </div>
            <div class="links">
            	<span class="linksIcon"></span>
                <a href="http://assist.batol.net/team/intro.asp" target="_blank" title="教育部視語障學習輔具中心">教育部視語障學習輔具中心</a>
            </div>
            <div class="links">
            	<span class="linksIcon"></span>
                <a href="http://cacd.nknu.edu.tw/cacd/default.aspx" target="_blank" title="教育部聽語障學習輔具中心">教育部聽語障學習輔具中心</a>
            </div>
            <div class="links">
            	<span class="linksIcon"></span>
                <a href="https://www.cter.edu.tw/" target="_blank" title="教育部職業轉銜資源網站">教育部職業轉銜資源網站</a>
            </div>
            <div class="links">
            	<span class="linksIcon"></span>
                <a href="http://www.daleweb.org/" target="_blank" title="教育部有愛無礙學障情障互動網站">教育部有愛無礙學障情障互動網站</a>
            </div>
            <div class="links">
            	<span class="linksIcon"></span>
                <a href="http://www.batol.net/" target="_blank" title="教育部無障礙全球資訊網">教育部無障礙全球資訊網</a>
            </div>
            <div class="links">
            	<span class="linksIcon"></span>
                <a href="http://www.edu.tw/Default.aspx?WID=65cdb365-af62-48cc-99d9-f9e2646b5b70" target="_blank" title="教育部學生事務及特殊教育司">教育部學生事務及特殊教育司</a>
            </div>
            <div class="links">
            	<span class="linksIcon"></span>
                <a href="http://repat.sfaa.gov.tw/ompt/page/" target="_blank" title="衛福部社會及家庭署矯具義具與行動輔具資源推廣中心">衛福部社會及家庭署矯具義具與行動輔具資源推廣中心</a>
            </div>
            <div class="links">
            	<span class="linksIcon"></span>
                <a href="http://www.aide.edu.tw/" target="_blank" title="國教署特教網路中心">國教署特教網路中心</a>
            </div>
        </span>
    </div>
    <div class="footer">
    	<div class="Info">
        	<?php
				if(isset($_SESSION['account'])){
        			echo '<a href="../logout.php">登出控制台</a>';
				}else{
					echo '<a href="?msg=login">主控台登入</a>';
				}
			?>
        	<label>700 台南市樹林街二段33號</label><br>
            <label>TEL: +886-6-2133111ext.645</label><br>
            <label>Copyright (c) 2017 Center of Special Education NUTN all rights reserved.</label>
        </div>
    </div>
    <?php //處理MessageBox部分
		if(isset($_GET['msg'])){
			$msg = htmlspecialchars($_GET['msg']);
			if($msg == 'login'){ //使用者點下"登入"
				if(isset($_SESSION['account'])){
					showMsgBox('loginsucess');
				}else{
					echo '<div class="messageBox">
						<form action="../login.php" method="post">
							<label id="msgBoxTitle">登入主控台</label>
							<input name="account" type="text" placeholder="請輸入工作人員帳號" autofocus required />
							<input name="password" type="password" placeholder="請輸入密碼" required />
							<a href="index.php" class="cancelBtn">取消</a>
							<input name="login" type="submit" value="登入" />
						</form>
					</div>';
				}
			}else if($msg == 'addnews'){
				if(isset($_SESSION['account'])){
					echo '<div class="messageBox">
						<form action="index.php" method="GET">
							<label id="msgBoxTitle">新增公告</label>
							<label class="inputLabel">這則公告會有多少項目?</label>
							<input name="numofnewsitem" type="number" placeholder="請輸入項目個數" min="1" autofocus required />
							<label class="inputLabel"><input name="isstudy" type="checkbox"/>研習公告</label>
							<a href="index.php" class="cancelBtn">取消</a>
							<input name="login" type="submit" value="確定" />
							<input type="hidden" name="msg" value="writenews" />
						</form>
					</div>';
				}else{
					showMsgBox('accessdenied');
				}
			}else if($msg == 'writenews'){
				if(isset($_GET['numofnewsitem']) and is_numeric($_GET['numofnewsitem'])){
					$numofnewsitem = $_GET['numofnewsitem'];
					echo '<div class="messageBox LargeBox">
							<div class="msgBoxMaskBlock">
								<form action="../addnews.php" method="post" enctype="multipart/form-data">
									<label id="msgBoxTitle">撰寫公告內容</label>
									<div class="inputGroup">
										<label class="inputGroupLabel">公告日期:</label>
										<input type="date" name="newsdate" autofocus required />
									</div>';
									if(isset($_GET['isstudy'])){
										echo '<input name="type" type="hidden" value="study">';
									}else{
										echo '<input name="type" type="hidden" value="general">';
									}
					for($i = 0;$i<$numofnewsitem;$i++){
						echo '
											<div class="inputGroup">
													<label class="inputGroupLabel">項目'.($i+1).'</label>
													<input name="newstitle[]" type="text" placeholder="請輸入標題" required />
													<input name="newscontent[]" type="text" placeholder="請輸入超連結(若要上傳檔案則無需填寫)" />
													<input name="file[]" type="file" value="上傳檔案" />
												</div>';
					}
					echo '			<a href="index.php" class="cancelBtn">取消</a>
								<input name="login" type="submit" value="確定" />
							</form>
						</div>
					</div>';
				}else{
					showMsgBox('nodata');
				}
			}else if($msg == 'delnews'){
				if(isset($_GET['delnewsno'])){
					$delnewsno = addslashes($_GET['delnewsno']);
					echo '<div class="messageBox">
						  <label id="msgBoxTitle">刪除公告</label><br>
						  <label>確定要刪除這則公告嗎?</label><br>
						  <label>(按下確定後將無法復原!)</label><br>
						  <a href="index.php" class="cancelBtn">取消</a>
						  <a href="../delnews.php?delnewsno='.$delnewsno.'" class="cancelBtn">確定</a>
					</div>';
				}else{
					showMsgBox('nodata');
				}
			}else{
				showMsgBox($msg);
			}
		}
	?>
</body>
</html>