<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<!doctype html>
<html lang="zh-Hant-TW">

<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="./CSS/indexnew.css" />
	<link rel="stylesheet" type="text/css" href="./CSS/output.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>臺南大學資源教室-
		<?php
		if (isset($_GET['page'])) {
			switch ($_GET['page']) {
				case "introduction":
					echo '教室簡介';
					break;
				case "manager":
					echo '管理運作';
					break;
				case "members":
					echo '成員執掌';
					break;
				case "collection":
					echo '叢書';
					break;
				case "services":
					echo '各項服務';
					break;
				case "aboutResourceRoom":
					echo '關於資源教室';
					break;
				case "siteMap":
					echo '網站導覽';
					break;
				case "current":
					echo '關於現況';
					break;
				case "others":
					echo '其他';
					break;
				case "downloads":
					echo '各類表格';
					break;
				case "links":
					echo '網路資源';
					break;
				case "laws":
					echo '相關法規';
					break;
				default:
					echo '首頁';
					break;
			}
		} else if (isset($_GET['activities'])) {
			echo '研習活動';
		} else {
			echo '最新消息';
		}

		?>
		National University of Tainan , Special Education Center , NUTN</title>
</head>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$DBname = "spedcenter";
require_once("../Functions/SQLFunctions.php");
session_start();
$connection = conncetSQLDB($DBname);
?>

<body>
	<div class="fixBar shadow-md">
		<a href="#main_content" id="toMain">跳到主要內容</a>
		<div class="fixBarContent">
			<a href="../../" alt="回到首頁(Go to home page)" style="width:unset">
				<span class="fixBarTitle">
					<img src="./Image/zh-tw-Title.png" alt="回到首頁(Go to home page)" />
				</span>
			</a>
			<span id="Navi" class="fixBarList">
				<span class="fixBarItem text-xl">
					<a class="Brick" accesskey="N" name="N" href="#N" title="導覽列">:::</a>
				</span>
				<span class="fixBarItem text-xl">
					<a href="../../">首頁</a>
				</span>
				<span class="fixBarItem text-xl">
					<a href="?page=siteMap">網站導覽</a>
				</span>
				<span class="fixBarItem text-xl">
					<a href="index.php">最新消息</a>
				</span>
				<span class="fixBarItem text-xl">
					<a href="?page=aboutResourceRoom">關於資源教室</a>
					<span class="selectionList">
						<span class="selections">
							<a href="?page=introduction">教室簡介</a>
						</span>
						<span class="selections">
							<a href="?page=members">成員執掌</a>
						</span>
						<span class="selections">
							<a href="?page=manager">管理運作</a>
						</span>
					</span>
				</span>
				<span class="fixBarItem text-xl">
					<a href="?page=services">各項服務</a>
					<!--<span class="selectionList">
            	<span class="selections">
                	<a href="?page=service_1">生活輔導</a>
                </span>
            	<span class="selections">
                	<a href="?page=service_2">學業輔導</a>
                </span>
            	<span class="selections">
                	<a href="?page=service_3">社會適應</a>
                </span>
            	<span class="selections">
                	<a href="?page=service_4">轉銜服務</a>
                </span>
            	<span class="selections">
                	<a href="?page=service_5">心理服務</a>
                </span>
            	<span class="selections">
                	<a href="?page=service_6">輔具借用與申請</a>
                </span>
            	<span class="selections">
                	<a href="?page=service_7">特教宣傳</a>
                </span>
            </span>-->
				</span>
				<span class="fixBarItem text-xl">
					<a href="?page=current">關於現況</a>
					<!--<span class="selectionList">
            	<span class="selections">
                	<a href="?page=service_1_c">生活輔導</a>
                </span>
            	<span class="selections">
                	<a href="?page=service_2_c">學業輔導</a>
                </span>
            	<span class="selections">
                	<a href="?page=service_3_c">社會適應</a>
                </span>
            	<span class="selections">
                	<a href="?page=service_4_c">轉銜服務</a>
                </span>
            	<span class="selections">
                	<a href="?page=service_5_c">心理服務</a>
                </span>
            	<span class="selections">
                	<a href="?page=service_6_c">輔具借用與申請</a>
                </span>
            	<span class="selections">
                	<a href="?page=service_7_c">特教宣傳</a>
                </span>
            </span>-->
				</span>
				<span class="fixBarItem text-xl">
					<a href="?page=others">其他</a>
					<span class="selectionList">
						<span class="selections">
							<a href="?page=downloads">各類表格</a>
						</span>
						<span class="selections">
							<a href="?page=laws">相關法規</a>
						</span>
						<span class="selections">
							<a href="?page=links">網路資源</a>
						</span>
						<span class="selections">
							<a href="mailto:iamcici@mail.nutn.edu.tw">與我們聯絡</a>
						</span>
					</span>
				</span>
			</span>
		</div>
	</div>
	<div class="mainImage">
		<div>
			<span class="otherItem text-xl">
				<a href="http://www.nutn.edu.tw/index-ch.htm">臺南大學</a>
				<a href="../../zh-tw">特教中心</a>
				<a href="../us-en/">English Version</a>
			</span>
		</div>
	</div>
	<a id="mainBrick" class="Brick text-xl" href="#M" name="M" accesskey="M" title="主要內容區域">:::</a>
	<div id="main_content" class="mainBlock bg-pink-100">
		<!--
	<h1>最新消息</h1>
    <div class="mainBlockContent">
        <a href="google.com">
    		<span class="newsBlocks">
                <div class="newsImage">
                	<img src="Image/諮詢專線-海報.jpg"/>
                </div>
                <div class="newsContent">
                    <div class="newsTitle">
                        <label>官網正式上線</label>
                    </div>
                    <div class="newsDate">
                        <label>2017-9-9</label>
                    </div>
                </div>
        	</span>
        </a>
    </div>
-->
		<?php
		if (isset($_GET['page'])) {	//固定頁面
			echoFixPage($_GET['page'], $connection);
		} else if (isset($_GET['NewsNo'])) {
			echoNews($connection, $_GET['NewsNo']);
		} else if (isset($_GET['addNews'])) {
			echoFixPage('addNews', $connection);
		} else if (isset($_GET['modnewsno'])) {
			echoModPage($connection, $_GET['modnewsno']);
		} else if (isset($_GET['modfix']) and is_numeric($_GET['modfix'])) {
			echoModFix($_GET['modfix'], $connection);
		} else if (isset($_GET['modmember']) and is_numeric($_GET['modmember'])) {
			echoModMember($_GET['modmember'], $connection);
		} else if (isset($_GET['modform']) and is_numeric($_GET['modform'])) {
			echoModForm($_GET['modform'], $connection);
		} else if (isset($_GET['modlaw']) and is_numeric($_GET['modlaw'])) {
			echoModLaw($_GET['modlaw'], $connection);
		} else if (isset($_GET['modlink']) and is_numeric($_GET['modlink'])) {
			echoModLink($_GET['modlink'], $connection);
		} else {		//總覽消息
			$newsPage = 1;
			if (isset($_GET['newsPage']) and is_numeric($_GET['newsPage'])) {
				if ($_GET['newsPage'] > 0) {
					$newsPage = $_GET['newsPage'];
				}
			}
			if (isset($_GET['activities'])) {
				echo '<h1>研習活動</h1>';
				$result = mysqli_query($connection, "SELECT * FROM CSE_News_RR ORDER BY Date DESC");
				if (mysqli_num_rows($result) != 0) {
					$row = mysqli_fetch_assoc($result);
					echo '<h4 class="lastUpdate">最後更新:' . $row['Date'] . '</h4>';
				}
				$result = mysqli_query($connection, "SELECT * FROM CSE_News_RR WHERE Type='1' ORDER BY Date DESC limit " . (($newsPage - 1) * 15) . ",15");
			} else {
				echo '<h1>最新消息</h1>';
				$result = mysqli_query($connection, "SELECT * FROM CSE_News_RR ORDER BY Date DESC");
				if (mysqli_num_rows($result) != 0) {
					$row = mysqli_fetch_assoc($result);
					echo '<h4 class="lastUpdate">最後更新:' . $row['Date'] . '</h4>';
				}
				$result = mysqli_query($connection, "SELECT * FROM CSE_News_RR ORDER BY Date DESC limit " . (($newsPage - 1) * 15) . ",15");
			}
			$newsRemain = 0;
			if (($newsRemain = mysqli_num_rows($result)) == 0 and $newsPage == 1) {
				echo '<div class="mainBlockContent">
        <a>
    		<span class="newsBlocksWrap">
                <div class="newsBlocks">
				<div class="newsContent">
                    <div class="newsTitle">
                        <label>目前尚未有任何新聞</label>
                    </div>
                </div>
				</div>
        	</span>
        </a>
    </div>';
			} elseif (($newsRemain = mysqli_num_rows($result)) == 0 and $newsPage != 1) {
				echo '<div class="mainBlockContent">
        <a href="?newsPage=1">
    		<span class="newsBlocksWrap">
                <div class="newsBlocks">
				<div class="newsContent">
                    <div class="newsTitle">
                        <label>已達最尾頁，點此回到第一頁</label>
                    </div>
                </div>
				</div>
        	</span>
        </a>';

				echo '<div class="pageSelect">';
				$act_str = '';
				if (isset($_GET['activities'])) {
					$act_str = 'activities=1';
				}
				if ($newsPage > 1) {
					echo '<span><a href="?newsPage=' . ($newsPage - 1) . $act_str . '">上一頁</a></span>';
				}
				echo '<span><label>第' . $newsPage . '頁</label></span>';
				if ($newsRemain == 15) {
					echo '<span><a href="?newsPage=' . ($newsPage + 1) . $act_str . '">下一頁</a></span>';
				}
				echo '</div></div>';
			} else {
				echo '<div class="mainBlockContent">';
				while ($row = mysqli_fetch_assoc($result)) {
					// echo '<span class="newsBlocksWrap"><a href="?NewsNo='.$row['No'].'">';
					// foreach(explode(';',$row['File']) as $filename){
					// 	if(file_exists("../uploads/".$filename) and substr(mime_content_type("../uploads/".$filename),0,5)=="image"){
					// 		echo '<div class="newsImage">
					// 		<img src="../uploads/'.$filename.'" alt="'.$row['Title'].'(附圖)-'.$filename.'"/>
					// 		</div>';
					// 		break;
					// 	}
					// }
					// echo '<div class="newsBlocks">';
					// echo '<div class="newsContent">
					//     <div class="newsTitle">
					//         <label>'.$row['Title'].'</label>
					//     </div>
					//     <div class="newsDate">
					//         <label>'.$row['Date'].'</label>
					//     </div>';
					// echo '</div>
					// </div>
					// </a>
					// </span>';
		?>
					<a href="?NewsNo=<?php echo $row['No'] ?>">
						<div class="bg-white mx-auto max-w-xs sm:max-w-lg lg:max-w-4xl min-w-5xl shadow hover:shadow-lg rounded-lg overflow-hidden rounded-full hover:bg-blue-100 m-4">
							<!-- <div class="sm:flex sm:items-center"> -->
							<div class="flex justify-center sm:justify-start flex-row flex-wrap px-6">
								<div class="text-center m-1">
									<img src="./Image/news-icon.png" title="news-icon" alt="news-icon" />
								</div>
								<div class=" text-orange-700 text-center px-4 m-1">
									<?php echo $row['Date'] ?>
								</div>
								<div class="text-center px-4 m-1">
									【<?php
										if ($row["Type"] == 1) {
											echo "研習活動";
										} else {
											echo "一般公告";
										}
										?>】
								</div>
								<div class="text-center px-4 m-1">
									<?php echo $row['Title'] ?>
								</div>
							</div>
							<!-- </div> -->
						</div>
					</a>
		<?php
				}
				echo '<div class="pageSelect">';
				$act_str = '';
				if (isset($_GET['activities'])) {
					$act_str = '&activities=1';
				}
				if ($newsPage > 1) {
					echo '<span><a href="?newsPage=' . ($newsPage - 1) . $act_str . '">上一頁</a></span>';
				}
				echo '<span><label>第' . $newsPage . '頁</label></span>';
				if ($newsRemain == 15) {
					echo '<span><a href="?newsPage=' . ($newsPage + 1) . $act_str . '">下一頁</a></span>';
				}
				echo '</div></div>';
			}
		}
		?>
	</div>
	<div class="footer">
		<span class="footerContent">
			<span class="about">
				<h5>相關資訊</h5>
				<p>700 台南市樹林街二段33號<br>
					TEL: +886-6-2133111ext.645<br>
					Copyright (c) 2017 Center of Special Education NUTN all rights reserved.
				</p>
			</span>
			<span class="panel">
				<h5>主控台</h5>
				<p>
					<?php
					if (isset($_SESSION['account'])) {
						echo '<a href="../logout.php">登出</a>';
					} else {
						echo '<a href="?msg=login">登入</a>';
					}
					?>
				</p>
			</span>
		</span>
	</div>
	<?php
	if (isset($_SESSION['account'])) {
		echo '<div class="floatingPanel">
				<span class="floatingControls">
					<a href="?addNews=1">新增公告</a>
				</span>
			</div>';
	}
	?>
	<?php //處理MessageBox部分
	if (isset($_GET['msg'])) {
		$msg = htmlspecialchars($_GET['msg']);
		if ($msg == 'login') { //使用者點下"登入"
			if (isset($_SESSION['account'])) {
				showMsgBox('loginsucess');
			} else {
				echo '<div class="messageBox">
						<form action="../login.php" method="post">
							<label id="msgBoxTitle">登入主控台</label>
							<input tabindex="1" name="account" type="text" placeholder="請輸入工作人員帳號" autofocus required />
							<input tabindex="2" name="password" type="password" placeholder="請輸入密碼" required />
							<a tabindex="4" href="index.php" class="cancelBtn">取消</a>
							<input tabindex="3" name="login" type="submit" value="登入" />
						</form>
					</div>';
			}
		} else if ($msg == 'delnews') {
			if (isset($_GET['delnewsno']) and is_numeric($_GET['delnewsno'])) {
				$delnewsno = addslashes($_GET['delnewsno']);
				echo '<div class="messageBox">
						  <label id="msgBoxTitle">刪除公告</label><br>
						  <label>確定要刪除這則公告嗎?</label><br>
						  <label>(按下確定後將無法復原!)</label><br>
						  <a href="index.php" class="cancelBtn">取消</a>
						  <a href="../delnews.php?delnewsno=' . $delnewsno . '" class="cancelBtn">確定</a>
					</div>';
			} else {
				showMsgBox('nodata');
			}
		} else if ($msg == 'delmember') {
			if (isset($_GET['delmemberno']) and is_numeric($_GET['delmemberno'])) {
				echo '<div class="messageBox">
						  <label id="msgBoxTitle">刪除成員資料</label><br>
						  <label>確定要刪除這名成員資料嗎?</label><br>
						  <label>(按下確定後將無法復原!)</label><br>
						  <a href="index.php" class="cancelBtn">取消</a>
						  <a href="../delmember.php?delmemberno=' . $_GET['delmemberno'] . '" class="cancelBtn">確定</a>
					</div>';
			} else {
				showMsgBox('nodata');
			}
		} else if ($msg == 'delform') {
			if (isset($_GET['delformno']) and is_numeric($_GET['delformno'])) {
				echo '<div class="messageBox">
						  <label id="msgBoxTitle">刪除下載表格</label><br>
						  <label>確定要刪除這個下載表格嗎?</label><br>
						  <label>(按下確定後將無法復原!)</label><br>
						  <a href="index.php" class="cancelBtn">取消</a>
						  <a href="../delform.php?delformno=' . $_GET['delformno'] . '" class="cancelBtn">確定</a>
					</div>';
			} else {
				showMsgBox('nodata');
			}
		} else if ($msg == 'dellaw') {
			if (isset($_GET['dellawno']) and is_numeric($_GET['dellawno'])) {
				echo '<div class="messageBox">
						  <label id="msgBoxTitle">刪除相關法規</label><br>
						  <label>確定要刪除這個相關法規檔案嗎?</label><br>
						  <label>(按下確定後將無法復原!)</label><br>
						  <a href="index.php" class="cancelBtn">取消</a>
						  <a href="../dellaw.php?dellawno=' . $_GET['dellawno'] . '" class="cancelBtn">確定</a>
					</div>';
			} else {
				showMsgBox('nodata');
			}
		} else if ($msg == 'dellink') {
			if (isset($_GET['dellinkno']) and is_numeric($_GET['dellinkno'])) {
				echo '<div class="messageBox">
						  <label id="msgBoxTitle">刪除連結</label><br>
						  <label>確定要刪除這個連結嗎?</label><br>
						  <label>(按下確定後將無法復原!)</label><br>
						  <a href="index.php" class="cancelBtn">取消</a>
						  <a href="../dellink.php?dellinkno=' . $_GET['dellinkno'] . '" class="cancelBtn">確定</a>
					</div>';
			} else {
				showMsgBox('nodata');
			}
		} else {
			showMsgBox($msg);
		}
	}
	?>
</body>

</html>