<?php //此程式純粹用來更改固定頁面內容到資料庫 請勿另作其他用途
	$DBname = "spedcenter";
    require_once("Functions/SQLFunctions.php");
	session_start();
	$connection = conncetSQLDB($DBname);
	ini_set("display_errors", "On"); 
	error_reporting(E_ALL & ~E_NOTICE);
	if(isset($_SESSION['account'])){
		if(isset($_POST['Content']) and isset($_POST['ModFixNo'])){
			$modfixno = $_POST['ModFixNo'];
			$content = htmlspecialchars($_POST['Content']);
			if(mysqli_query($connection,"UPDATE CSE_Fix_Contents_RR SET Content='".$content."' WHERE No='".$modfixno."'")){
				header("Location: zh-tw/index.php?msg=modfixsuccess");
			}else{
				header("Location: zh-tw/index.php?msg=modfixfailed");
			}
			
		}elseif(isset($_POST['ModFixNo']) and isset($_FILES['Image'])){
			$modfixno = $_POST['ModFixNo'];
			$upsuc="F";
			$filename="";
			if($_FILES['Image']['error']==UPLOAD_ERR_OK){//good file
				if (move_uploaded_file($_FILES['Image']['tmp_name'],"uploads/".$_FILES['Image']['name'])) {
					$filename = $_FILES['Image']['name'];
					$upsuc="T";
				}else{
					$upsuc="F";
				}
			}elseif($_FILES['Image']['error']==UPLOAD_ERR_NO_FILE){
				$upsuc="NoUpload";
			}else{
				$upsuc="F";
			}
			if($upsuc=="T"){
				if(mysqli_query($connection,"UPDATE CSE_Fix_Contents_RR SET Content='".$filename."' WHERE No='".$modfixno."'")){
					header("Location: zh-tw/index.php?msg=modfixsuccess");
				}else{
					header("Location: zh-tw/index.php?msg=modfixfailed");
				}
			}else{
				header("Location: zh-tw/index.php?msg=addnewssucuploadfailed");
			}
		}else{
			header("Location: zh-tw/index.php?msg=nodata");
		}
	}else{
		header("Location: zh-tw/index.php?msg=accessdenied");
	}
?>