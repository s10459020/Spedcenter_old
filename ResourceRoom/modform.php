<?php //此程式純粹用來更改各式表格內容到資料庫 請勿另作其他用途
	$DBname = "spedcenter";
    require_once("Functions/SQLFunctions.php");
	session_start();
	$connection = conncetSQLDB($DBname);
	ini_set("display_errors", "On"); 
	error_reporting(E_ALL & ~E_NOTICE);
	if(isset($_SESSION['account'])){
		if(isset($_POST['ModFormNo']) and isset($_POST['Name'])){
			$modformno = $_POST['ModFormNo'];
			$name = htmlspecialchars($_POST['Name']);
			$upsuc="F";
			$filename="";
			if($_FILES['File']['error']==UPLOAD_ERR_OK){//good file
				if (move_uploaded_file($_FILES['File']['tmp_name'],"uploads/".$_FILES['File']['name'])) {
					$filename = $_FILES['File']['name'];
					$upsuc="T";
				}else{
					$upsuc="F";
				}
			}elseif($_FILES['File']['error']==UPLOAD_ERR_NO_FILE){
				$upsuc="NoUpload";
			}else{
				$upsuc="F";
			}
			if($upsuc=="T"){
				if(mysqli_query($connection,"UPDATE CSE_Forms_RR SET Name='".$name."',FileName='".$filename."' WHERE No='".$modformno."'")){
					header("Location: zh-tw/index.php?msg=modfixsuccess");
				}else{
					header("Location: zh-tw/index.php?msg=modfixfailed");
				}
			}elseif($upsuc=="NoUpload"){
				if(mysqli_query($connection,"UPDATE CSE_Forms_RR SET Name='".$name."' WHERE No='".$modformno."'")){
					header("Location: zh-tw/index.php?msg=modfixsuccess");
				}else{
					header("Location: zh-tw/index.php?msg=modfixfailed");
				}
			}else{
				header("Location: zh-tw/index.php?msg=addnewscontentfailed");
			}
		}else{
			header("Location: zh-tw/index.php?msg=nodata");
		}
	}else{
		header("Location: zh-tw/index.php?msg=accessdenied");
	}
?>