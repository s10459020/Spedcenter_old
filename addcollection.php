<?php 
$DBname = "spedcenter";
require_once("Functions/SQLFunctions.php");
session_start();
$conncetion = conncetSQLDB($DBname);
ini_set("display_errors", "On"); // 顯示錯誤是否打開( On=開, Off=關 )
error_reporting(E_ALL & ~E_NOTICE);
$upsuc = "T";
if (isset($_SESSION['account'])) {
	$class = "";
	if (isset($_POST['Class'])) {
		$class = $_POST['Class'];
	}
	$author = "";
	if (isset($_POST['Author'])) {
		$author = $_POST['Author'];
	}
	$bookName = "";
	if (isset($_POST['BookName'])) {
		$bookName = $_POST['BookName'];
	}
	$remarks = "";
	if (isset($_POST['Remarks'])) {
		$remarks = $_POST['Remarks'];
	}
	$filename = '';

	//處理檔案 
	if ($_FILES['Upload']['error'] != 4 /*and is_uploaded_file($_FILES['Upload']['tmp_name'])*/) { //該項目為檔案 且格式合格
		if ($_FILES['Upload']['error'] > 0) {
			$upsuc = "F";
		} else {
			if (move_uploaded_file($_FILES['Upload']['tmp_name'], "collection_appends/" . iconv( 'utf-8', 'big5', $_FILES['Upload']['name']))) {
				$filename = $_FILES['Upload']['name'];
				$upsuc = "T";
			} else {
				$upsuc = "F";
			}
		}
	} else {
		$upsuc = "NoUpload";
	}

	if ($upsuc == "NoUpload") {
		if (mysqli_query($conncetion, "INSERT INTO CSE_Collection (Class,Author,BookName,Remarks)VALUES('" . $_POST['Class'] . "','" . $_POST['Author'] . "','" . $_POST['BookName'] . "','" . $_POST['Remarks'] . "')")) {
			header("Location: zh-tw/index.php?msg=addcollectionsuccess");
		} else {
			header("Location: zh-tw/index.php?msg=addcollectioncontentfailed");
		}
	} else {
		if (mysqli_query($conncetion, "INSERT INTO CSE_Collection (Class,Author,BookName,Remarks,File)VALUES('" . $_POST['Class'] . "','" . $_POST['Author'] . "','" . $_POST['BookName'] . "','" . $_POST['Remarks'] . "','" . $filename . "')")) {
			if ($upsuc == "T") {
				header("Location: zh-tw/index.php?msg=addcollectionsuccess");
			} else {
				header("Location: zh-tw/index.php?msg=addcollectionsucuploadfailed");
			}
		} else {
			header("Location: zh-tw/index.php?msg=addcollectioncontentfailed");
		}
	}
} else {
	header("Location: zh-tw/index.php?msg=accessdenied");
}
